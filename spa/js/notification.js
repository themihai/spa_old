$notification = {
	init: function () {
		var w = $('<div id="notification"></div>').appendTo('body').slideUp(0);
		var c = $('<div></div>');
		var t_o;
		this.show = function (type, text, hide) {
			clearTimeout(t_o);
			if (!w.hasClass(type)) {
				w.slideUp(0);
			}
			w.attr('class', type);
			c.text(text).appendTo(w);
			w.slideDown();
			if (hide) {
				t_o = setTimeout(function () {
					$notification.close();
				}, 3000);
			}
		}
		this.close = function () {
			clearTimeout(t_o);
			w.slideUp();
			
		}
	}
}
$notification.init();
