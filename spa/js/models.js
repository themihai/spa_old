var Product = function (data) {
	this.setData(data);
}
Product.prototype.setData = function (data) {
	if (typeof data.ItemID !== 'undefined') {
		//let's asume this is getSingleItemV2 data
		this.singleItemData = data;

		//need to set some bools and arrays for handlebars that can't be prototype methods, they have to be actual properties.
		this.hasBids = this.singleItemData.ListingType == 'Auction' || this.singleItemData.ListingType == 'AuctionWithBIN';
		this.bidCount = this.singleItemData.BidCount;
		this.hasBIN = this.singleItemData.ListingType == 'FixedPriceItem' || this.singleItemData.ListingType == 'AuctionWithBIN';
		this.paymentMethods = this.singleItemData.PaymentMethods;
		this.needsPager = this.singleItemData.PictureURL.length > 1 || this.singleItemData.PictureURL.length > 0 && this.singleItemData.VideoId && this.singleItemData.PictureURL[0].indexOf('no_photo') == -1;

	} else {
		//only other case for now is search results data
		this.searchData = data;
		this.hasBids = this.searchData.listingInfo[0].listingType[0] == 'Auction' || this.searchData.listingInfo[0].listingType[0] == 'AuctionWithBIN';
	}

	//need to set some bools for handlebars that can't be prototype methods, they have to be actual properties.
	this.isActive = (this.singleItemData ? this.singleItemData.ListingStatus : this.searchData.sellingStatus[0].sellingState[0]) == 'Active';

}
Product.prototype.setLocation = function (geo) {
	this.geolocation = geo;
}
Product.prototype['location'] = function () {
	return this.geolocation ? this.geolocation.city + ', ' + this.geolocation.state : '';
}
Product.prototype.itemId = function () {
	return this.singleItemData ? this.singleItemData.ItemID : this.searchData.itemId[0];
}
Product.prototype.title = function () {
	return this.singleItemData ? this.singleItemData.Title : this.searchData.title[0];
}
Product.prototype.price = function () {
	return this.singleItemData ? this.singleItemData.CurrentPrice : this.searchData.sellingStatus[0].convertedCurrentPrice[0].__value__;
}
Product.prototype.minimumBid = function () {
	return this.singleItemData.MinimumToBid;
}
Product.prototype.image = function (o) {
	var src = this.singleItemData ? this.singleItemData.PictureURL[0] : this.searchData.galleryURL[0];
	if (o && o.hash && o.hash.size && src.indexOf('no_photo') == -1) {
		src += '=s' + o.hash.size + (o.hash.crop ? '-c': '');
	}
	return src;
}
Product.prototype.images = function () {
	var imgs = '';
	if (this.singleItemData.VideoId) {
		imgs += '<li><img src="http://img.youtube.com/vi/' + this.singleItemData.VideoId + '/2.jpg" height=100 class="active video" data-videoid="' + this.singleItemData.VideoId + '"></li>';
	}
	for (var i = 0, l = this.singleItemData.PictureURL.length; i < l; i++) {
		var no_photo = this.singleItemData.PictureURL[i].indexOf('no_photo') != -1;
		if (!(no_photo && l == 1)) {
			imgs += '<li><img ' + (i == 0 && !this.singleItemData.VideoId? 'class="active" height=100 ' : 'height=88 ') + 'src="' + this.singleItemData.PictureURL[i] + (no_photo ? '' : '=s100') + '" data-big="' + this.singleItemData.PictureURL[i] + (no_photo ? '' : '=s640') + '" data-huge="' + this.singleItemData.PictureURL[i] + (no_photo ? '' : '=s1600') + '"></li>' ;
		}
	}

	return '<ul' + (this.needsPager ? '' : ' style="display: none;"') + '>' + imgs + '</ul>';
}
Product.prototype.galleryImage = function (o) {
	if (this.singleItemData.VideoId) {
		return '<iframe frameborder=0 width="100%" height="100%" src="http://www.youtube.com/embed/' + this.singleItemData.VideoId + '?wmode=opaque&modestbranding=1&autohide=1&color=red&hd=1&rel=0&showinfo=0&theme=light"></iframe>';
	} else {
		var src = this.singleItemData.PictureURL[0];
		if (src.indexOf('no_photo') == -1) {
			src += '=s640';
		}
		return '<img src="' + src + '" height=320>';
	}
}
Product.prototype.url = function () {
	return this.seller() + '/' + this.slug();
}
Product.prototype.realUrl = function () {
	return 'http://www.epek.com/' + this.url();
}
Product.prototype.seller = function () {
	return this.singleItemData ? this.singleItemData.Seller.UserID : this.searchData.sellerInfo[0].sellerUserName[0];
}
Product.prototype.slug = function () {
	return this.singleItemData ? this.singleItemData.TitleSlug : this.searchData.titleSlug[0];
}
Product.prototype.hasSingleItemData = function () {
	return typeof this.singleItemData !== 'undefined';
}
Product.prototype.hasSearchData = function () {
	return typeof this.searchData !== 'undefined';
}
Product.prototype.ends = function () {
	var d = this.singleItemData ? this.singleItemData.EndTime : this.searchData.titleSlug[0];
	d = new Date(d);
	return (this.isActive ? 'Ends ' : 'Ended ') + d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
}
Product.prototype.timeLeft = function (o) {
	if (!this.isActive) {
		return this.hasBids ? 'This auction has ended' : 'This item has been sold';
	}
	var abbr = o && o.hash && typeof o.hash.abbr !== 'undefined' ? o.hash.abbr : false;
	var t = this.singleItemData ? this.singleItemData.TimeLeft : this.searchData.sellingStatus[0].timeLeft[0];
	var abbr = typeof abbr === 'boolean' ? abbr : false;
	var days = t.match(/([\d]*)D/);
	if (days) {
		days = days[1];
		var ds = abbr ? 'd' : ' day' + (days == 1 ? '' : 's');
	}
	var hours = t.match(/([\d]*)H/);
	if (hours) {
		hours = hours[1];
		var hs = abbr ? 'h' : ' hour' + (hours == 1 ? '' : 's');
	}
	var minutes = t.match(/([\d]*)M/);
	if (minutes) {
		minutes = minutes[1];
		var ms = abbr ? 'm' : ' minute' + (minutes == 1 ? '' : 's');
	}
	if (days) {
		if (hours) {
			var time_left = days + ds + ' ' + (abbr ? '' : 'and ') + hours + hs + ' left';
		} else if (minutes) {
			var time_left = days + ds + ' ' + (abbr ? '' : 'and ') + minutes + ms + ' left';
		} else {
			var time_left = days + ds + ' left';
		}
	} else if (hours) {
		if (minutes) {
			var time_left = hours + hs + ' ' + (abbr ? '' : 'and ') + minutes + ms + ' left';
		} else {
			var time_left = hours + hs + ' left';
		}
	} else if (minutes) {
		var time_left = minutes + ms + ' left!';
	} else {
		var time_left = 'Seconds left!';
	}
	return time_left;
}
Product.prototype.shippingPrice = function () {
	return this.singleItemData ? this.singleItemData.ShippingMethods[0].Cost : this.searchData.shippingInfo[0].shippingServiceCost[0].__value__;
}
Product.prototype.shippingName = function () {
	return this.singleItemData ? this.singleItemData.ShippingMethods[0].Name : this.searchData.shippingInfo[0].shippingName[0];
}
Product.prototype.shippingMethods = function () {
	var methods = [];
	var ormethods = this.singleItemData.ShippingMethods;
	if (ormethods) {
		var now = new Date().getTime();
		var min = new Date();
		var max = new Date();
		var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		for (var i = 0, l = ormethods.length; i < l; i++) {
			var m = {
				'cost': ormethods[i].Cost == '0.00' ? '<b>FREE</b>' : '$<b>' + ormethods[i].Cost + '</b>',
				'name': ormethods[i].Name
			}
			if (ormethods[i].DeliveryEstimateMin == ormethods[i].DeliveryEstimateMax) {
				min.setTime(now + (parseInt(ormethods[i].DeliveryEstimateMin, 10) * 86400000));
				m.delivery = 'By ' + months[min.getMonth()] + ' ' + min.getDate();
			} else {
				min.setTime(now + (parseInt(ormethods[i].DeliveryEstimateMin, 10) * 86400000));
				max.setTime(now + (parseInt(ormethods[i].DeliveryEstimateMax, 10) * 86400000));
				m.delivery = months[min.getMonth()] + ' ' + min.getDate() + ' – ' + months[min.getMonth()] + ' ' + max.getDate();
			}
			methods.push(m);
		}
	}
	return methods;
}
Product.prototype.listingType = function () {
	return this.singleItemData ? this.singleItemData.ListingType : this.searchData.listingInfo[0].listingType[0];
}
Product.prototype.setProfile = function (profile) {
	this.profile = profile;
}
Product.prototype.sellerFeedback = function () {
	return this.profile.sellerFeedback();
}
Product.prototype.sellerScore = function () {
	return this.profile.sellerScore();
}
Product.prototype.quantitySelect = function () {
	var q = this.singleItemData.QuantityAvailable;
	var r = '';
	if (q > 1) {
		r += '<label>Quantity <select name=quantity>';
		for (var i = 1; i <= q; i++) {
			r += '<option value=' + i + '>' + i + '</option>';
		}
		r += '</select></label>';
	}
	return r;
}
Product.prototype.temptativeBid = function (bid) {
	if (bid.hash) {
		return this.temptativeBidValue || 0;
	} else {
		this.temptativeBidValue = bid;
	}
}
Product.prototype.updateWithBidReturn = function (bidReturn) {
	var d = this.singleItemData;
	var br = bidReturn.SellingStatus;
	d.BidCount = br.BidCount;
	this.bidCount = br.BidCount;
	d.CurrentPrice = br.CurrentPriceFormatted;
	d.HighBidder = br.HighBidder;
	d.ListingStatus = br.ListingStatus;
	d.MinimumToBid = br.MinimumToBidFormatted;
	d.ReserveMet = br.ReserveMet;
	delete this.bidHistory;
}
Product.prototype.fetchRelationship = function (cb) {
	if ($login.session && $login.session.valid) {
		var self = this;
		$.getJSON('http://offer.epek.com/api/getItemRelationship?sessionId=' + $login.sess_id + '&ItemID=' + this.itemId(), function (r) {
			self.relationship = r;
			cb.call(self);
		});
	}
}
Product.prototype.buyerRelationship = function () {
	var lastBid = new Date(this.relationship.Buyer.UserLastBid);
	var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
	if (this.relationship.Buyer.IsWinner) {
		return '<strong>Congratulations!</strong> You ' + (this.listingType() == 'FixedPriceItem' ? 'bought' : 'won') + ' this item! <button data-href="http://stack.epek.com/activity/#category=singleitem&item=' + this.itemId() + '"><span>Manage Your Purchase</span></button>';
	} else {
		return 'You bidded on this item on ' + months[lastBid.getMonth()] + ' ' + lastBid.getDate() + '.' + (this.singleItemData.HighBidder.UserID == $login.session.user ? ' <strong>You\'re currently the highest bidder.</strong> Hope you win!' : '<strong>You\'ve been outbid</strong>. Don\'t let it get away - bid again!');
	}
}
Product.prototype.sellerRelationship = function () {
	var added = new Date(this.singleItemData.StartTime);
	var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
	return 'You listed this item on ' + months[added.getMonth()] + ' ' + added.getDate() + '. You have sold ' + this.relationship.Seller.QuantitySold + ' of ' + this.relationship.Seller.QuantityAvailable + '.<button data-href="http://stack.epek.com/activity/#category=singleitem&item=' + this.itemId() + '"><span>Manage Sales</span></button>';
}
Product.prototype.watching = function () {
	if (this.relationship) {
		return this.relationship.Guest.IsWatching ? 'Watching' : 'Watch';
	} else {
		return 'Watch';
	}
}
Product.prototype.setWatching = function (watching) {
	this.relationship.Guest.IsWatching = watching;
}
Product.prototype.fetchBids = function (cb) {
	var self = this;
	if (typeof this.bidHistory === 'undefined') {
		$.getJSON('http://offer.epek.com/api/getBidHistory?ItemID=' + this.itemId(), function (bids) {
			self.bidHistory = bids.BidHistory.Bids;
			cb();
		});
	}
}

Handlebars.registerHelper('shippingMethods', function(options) {
	return options.fn({'methods': this.shippingMethods()});
});
Handlebars.registerHelper('biddersList', function(options) {
	if (this.bidHistory) {
		return options.fn(this);
	} else {
		var ps = {bidHistory: []}
		for (var i = 0; i < this.bidCount; i++) {
			ps.bidHistory.push({'date': '-', 'name': '-', 'value': '-'})
		}
		return options.fn(ps);
	}
});
Handlebars.registerHelper('window_location', function(options) {
	return window.location.href;
});
Handlebars.registerHelper('has_social', function(options) {
	if ($social.has(options.hash.service)) {
		return 'selected';
	}
});
Handlebars.registerHelper('bid_date', function(date) {
	if (date == '-') {
		return date;
	} else {
		var d = new Date(date);
		return d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
	}
});




var Profile = function (id, cb, full) {
	var self = this, cb = cb || function () {};

	this.id = id;
	this.feedback = {r1: [], r2: [], r3: [], r4: []};
	this.isUser = $login.username == id;
	var pending = 0;
	
	$.getJSON('http://feedback.api.epek.com/api/feedback/user/stats?username=' + id, function (r) {
		self.stats = r;
		if (full) {
			self.fetchFullProfile(cb);
		} else {
			cb();
		}
	});
}
Profile.prototype.descriptionText = function () {
	return this.isUser && this.description == '' ? 'About yourself in fewer than 122 characters including spaces.' : this.description;
}
Profile.prototype.sellerScore = function () {
	return this.stats.scoreAsSeller;
}
Profile.prototype.sellerFeedback = function () {
	return this.stats.pPositiveAsSeller + '%';
}
Profile.prototype.fetchFullProfile = function (cb) {
	var _cb = cb || function () {};
	if (this.full) {
		_cb();
	} else {
		this.full = true;
		var count = 0;
		var self = this;
		this.questions = {
			'asked': '-',
			'answered': '-'
		};
		count++;
		$.getJSON('http://upload-profile.epek.com/service/getdescription?username=' + self.id, function (r) {
			if (r.STATUS == 'OK') {
				self.description = r.message;
			}
			count--;
			if (count == 0) {
				_cb();
			}
		});
		count++;
		$.getJSON('http://offer.epek.com/api/follow_stats?username=' + self.id, function (r) {
			self.follow = r;
		}).complete(function () {
			count--;
			if (count == 0) {
				_cb();
			}
		});
		if ($login.session && $login.session.valid) {
			count++;
			$.ajax({
				url:'http://offer.epek.com/api/get_relationship?username=' + self.id + '&SessionId=' + $login.sess_id,
				dataType: 'json',
				success: function (r) {
					self.relationship = r;
				},
				complete: function () {
					count--;
					if (count == 0) {
						_cb();
					}
				}
			});
		}
	}
}
Profile.prototype.fetchFeedbackItems = function (cb) {
	var _cb = cb || function () {};
	if (!this.hasFeedback) {
		var self = this;
		$.getJSON('http://feedback.api.epek.com/api/getUserFeedbackList?username=' + this.id, function (r) {
			self.hasFeedback = true;
			for (var i = 0, l = r.feedbacks.length; i < l; i++) {
				self.feedback['r' + r.feedbacks[i].feedbackRating].push(r.feedbacks[i]);
			}
			cb();
		});
	} else {
		_cb();
	}
}
Profile.prototype.fetchFollowingItems = function (cb) {
	var _cb = cb || function () {};
	var self = this;

	if (this.following) {
		cb();
	} else {
		$.getJSON('http://offer.epek.com/api/get_following_list?username=' + this.id, function (r) {
			$.getJSON('http://upload-profile.epek.com/service/getdescription?username=' + r.following.join(','), function (s) {
				var following = [];
				if (r.following.length > 1) {
					for (var i = 0, l = r.following.length; i < l; i++) {
						following.push({'name': r.following[i], 'description': s.message[r.following[i]]});
					}
				} else {
					following.push({'name': r.following[0], 'description': s.message});
				}
				self.following = following;
				cb();
			});
		});
	}
}
Profile.prototype.fetchFollowersItems = function (cb) {
	var _cb = cb || function () {};
	var self = this;
	if (this.followers) {
		cb();
	} else {
		$.getJSON('http://offer.epek.com/api/get_followers_list?username=' + this.id, function (r) {
			$.getJSON('http://upload-profile.epek.com/service/getdescription?username=' + r.followers.join(','), function (s) {
				var followers = [];
				if (r.followers.length > 1) {
					for (var i = 0, l = r.followers.length; i < l; i++) {
						followers.push({'name': r.followers[i], 'description': s.message[r.followers[i]]});
					}
				} else {
					followers.push({'name': r.followers[0], 'description': s.message});
				}
				self.followers = followers;
				cb();
			});
		});
	}
}
Handlebars.registerHelper('followingList', function(options) {
	if (this.following) {
		return options.fn(this);
	} else {
		var ps = {following: []}
		for (var i = 0; i < this.follow.following; i++) {
			ps.following.push(false);
		}
		return options.fn(ps);
	}
});
Handlebars.registerHelper('followersList', function(options) {
	if (this.followers) {
		return options.fn(this);
	} else {
		var ps = {followers: []}
		for (var i = 0; i < this.follow.followers; i++) {
			ps.followers.push(false);
		}
		return options.fn(ps);
	}
});
