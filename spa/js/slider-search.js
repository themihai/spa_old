$.fn.digits = function(){ 
    return this.each(function(){ 
        $(this).val( $(this).val().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") ); 
    });
}
function is_int(value){ 
    if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
        return true;
    } else { 
        return false;
    } 
}
function update_min_max_slider_indicator(ui) {
    $('#min_max').parents('.slide_area').find('.indicator').each(function(index) {
        $(this).find('.editable').val(isNaN(ui.values[index]) ? '-' : ui.values[index]).digits().trigger('update');
    });
}
function update_radius_slider_indicator(ui) {
	if (ui.value == $(this).slider('option', 'max')) {
		var v = '-';
	} else {
		var v = isNaN(ui.value) ? '-' : ui.value;
	}
    $('#radius').parents('.slide_area').find('.editable').val(v).trigger('update');
}
$('#slider').find('input').autoGrowInput();
(function($){
    $('#min_max').slider({
        range: true,
        animate: false,
        min: 0,
        max: 0,
        step: 1,
        values: [0, 0],
        create: function(event, ui) {
            //setTimeout(function() { update_min_max_slider_indicator({values: [0, 0]}) }, 500);
        },
        slide: function(event, ui) {
            update_min_max_slider_indicator(ui);
        },
        change: function(event, ui) {
		if ($spa.controllers.search.query.tag) {
			$spa.navigate('tag', $spa.controllers.search.query.tag);
		} else if ($spa.controllers.search.query.profile) {
			$spa.navigate('profile', $spa.controllers.search.query.profile);
		} else {
			$spa.navigate('search', $spa.controllers.search.query.keywords);
		}
            update_min_max_slider_indicator(ui);
        }
    });
    $('#radius').slider({
        range: 'min',
        animate: false,
        min: 1,
        max: 1000,
        step: 1,
        value: 1000,
        slide: function(event, ui) {
            update_radius_slider_indicator.call(this, ui);
        },
        change: function(event, ui) {
		if ($spa.controllers.search.query.tag) {
			$spa.navigate('tag', $spa.controllers.search.query.tag);
		} else if ($spa.controllers.search.query.profile) {
			$spa.navigate('profile', $spa.controllers.search.query.profile);
		} else {
			$spa.navigate('search', $spa.controllers.search.query.keywords);
		}
            update_radius_slider_indicator.call(this, ui);
        }
    });
    $('.indicator').on('mousedown', function(e) {
        e.stopPropagation();
        var edit = $(this).find('.editable');
        setTimeout(function() { edit.focus() }, 100);
    });
    var editable_t_o;
    $('.indicator.min span.value .editable, .indicator.max span.value .editable').keypress(function(e) {
	clearTimeout(editable_t_o);
	var $this = $(this);
        if(e.which == 13) {
            // enter pressed
            e.preventDefault();
	    parseEditableValues();
        } else {
		var self = this;
		editable_t_o = setTimeout(function () {
			parseEditableValues();
		}, 600)
	}
    }).blur(function () {
	clearTimeout(editable_t_o);
	parseEditableValues();
    });
    var parseEditableValues = function () {
        var o = $('#min_max');
        var values = [];
        
        $('#slider .editable').each(function() {
            var v = parseInt($(this).val().replace(',', ''));
            if (is_int(v)) {
                values.push(v);
            } else {
                values.push(0);
            }
        });
        values.sort(function(a, b) {
            return a - b;
        });
        if (values[1] > o.slider('option', 'max')) {
            values[1] = o.slider('option', 'max');
        }
        o.slider('option', { values: values });
    }
})( jQuery );
