$spa.controllers = {
	home: {
		data: {},
		queryURL: 'http://search.api.epek.com/query',
		init: function () {
			this.setupHandlers();
			$('#header form .real:text').val('');
			if (this.done) {
				$('#home').show();
				$spa.loading.hide();
				this.setupScrolling();
				$(window).scrollTop($('#home').data('st'));
			} else {
				var self = this;
				var count = 0;
				count++;
				$.getJSON(this.queryURL, {sortOrder: 'distance', distance: $spa.distance()}, function (r) {
						count--;
						self.data.results = r;
						if (count == 0) {
							self.parseData();
						}
				}).error(function (x, t) {
					$('#return').empty('').show();
					$spa.loading.hide();
					$('<p>' + t + '</p>').css({'text-align': 'center', 'font-size': '80px', 'color': '#369'}).appendTo('#return');
				});
				count++;
				$.getJSON('http://data.epek.com/US_EN/category', function (r) {
						count--;
						self.data.categories = $.map(r.meta.children, function (item) { return {'name': item.CategoryName, 'path': item.CategoryPath}; });
						if (count == 0) {
							self.parseData();
						}
				}).error(function (x, t) {
					$('#return').empty('').show();
					$spa.loading.hide();
					$('<p>' + t + '</p>').css({'text-align': 'center', 'font-size': '80px', 'color': '#369'}).appendTo('#return');
				});
			}
		},
		parseData: function () {
			this.done = true;

			this.items = this.data.results.findItemsAdvancedResponse[0].searchResult[0].item;
			this.tpl = Handlebars.compile($('#home-items-tpl').html());
			this.tplContext = {items: [], categories: this.data.categories};
			for (var i = 0, l = this.items.length > 5 ? 5 : this.items.length; i < l; i++) {
				this.tplContext.items.push(this.parseProduct(this.items[i]));
			}
			this.show();
		},
		show: function () {
			var reduce = function (w, h1, span) {
				span.text(span.text().slice(0, -1));
				if (h1.width() > w) {
					reduce(w, h1, span);
				}
			}
			$('#home').html(this.tpl(this.tplContext)).show().find('> ul li').each(function () {
				var h1 = $(this).find('h1');
				var w = $(this).width();
				if (w < h1.width()) {
					h1.append('…&nbsp;');
					var span = h1.find('span');
					span.attr('title', span.text());
					reduce(w, h1, span);
				}
			}).end().find('> div ul a strong').each(function () {
				var $this = $(this);
				$this.css('margin-left', -($this.outerWidth() / 2));
			});
			$spa.loading.hide();
			this.setupScrolling();
		},
		setupScrolling: function () {
			var self = this;

			var $f = $('#footer');
			var wh, ftop;
			self.sizes = function () {
				wh = $(window).height();
				ftop = $f.offset().top;
			}
			self.sizes();
			$(window).bind('scroll.loader', function () {
				if ($(this).scrollTop() > ftop - wh) {
					self.showMore();
				}
			});
			$(window).bind('resize.loader', function () {
				self.sizes();
			});
		},
		showMore: function () {
			var self = this;
			var cnt = $('#home .more');
			if (cnt.is('.loading')) {
				return;
			}
			if (!cnt.length) {
				cnt = $('<div class="more">').data('results', this.data.results).appendTo('#home');
				this.itemTpl = Handlebars.compile($('#home-item-tpl').html());
				var ul = $('<ul></ul>');
				for (var i = 5, l = this.items.length; i < l; i++) {
					ul.append(this.itemTpl(this.parseProduct(this.items[i])));
				}
				ul.appendTo(cnt);
				this.sizes();
			} else {
				var cursor = cnt.data('results').findItemsAdvancedResponse[0].paginationOutput[0].cursor[0];
				if (cursor !== '') {
					cnt.addClass('loading');
					$.getJSON(this.queryURL, {'cursor': cursor, sortOrder: 'distance', distance: $spa.distance()}, function (r) {
						var items = r.findItemsAdvancedResponse[0].searchResult[0].item;
						var ul = cnt.find('ul');
						cnt.data('results', r);
						for (var i = 0, l = items.length; i < l; i++) {
							ul.append(self.itemTpl(self.parseProduct(items[i])));
						}
						self.sizes();
					}).error(function (x, t) {
						$('#return').empty('').show();
						$spa.loading.hide();
						$('<p>' + t + '</p>').css({'text-align': 'center', 'font-size': '80px', 'color': '#369'}).appendTo('#return');
					}).complete(function () {
						cnt.removeClass('loading');
					});
				}
			}
		},
		parseProduct: function (item) {
			var product_url = item.sellerInfo[0].sellerUserName[0] + '/' + item.titleSlug[0];
			if (typeof $spa.controllers.product.data[product_url] === 'undefined') {
				$spa.controllers.product.data[product_url] = new Product(item);
			}
			return $spa.controllers.product.data[product_url];
		},
		setupHandlers: function () {
			if (!this.hasHandlers) {
				this.hasHandlers = true;
				$('#home').on('click', '.items li, .more li', function () {
					$('#home').data('st', $(window).scrollTop());
					window.location.href = $(this).data('url');
				});
			}
		}
	},
	search: {
		data: {},
		init: function (s, extra) {
			if (extra !== 'tag') {
				$('#wrapper').addClass('filter');
			}
			this.setupHandlers();
			var self = this;
			if (typeof this.tpl === 'undefined') {
				this.tpl = Handlebars.compile($('#search-items-tpl').html());
				this.itemTpl = Handlebars.compile($('#search-item-tpl').html());
			}
			this.parseQuery(s, extra);
		},
		parseQuery: function (s, extra, cursor) {
			var q = s.split('/');
			var query = {
				'minmax': ['', ''],
				'ListingType': 'All',
				'original': s,
				'category': '',
				'extra': extra,
				'sortOrder': 'distance',
				'miles': 0,
				'tag': ''
			}
			if (cursor) {
				query.cursor = cursor;
			}
			if (extra == 'profile') {
				query.keywords = '';
				query.profile = q.shift();
			} else if (extra == 'tag') {
				query.keywords = '';
				query.profile = '';
				query.tag = q.shift();
			} else {
				query.keywords = decodeURIComponent(q.shift());
				query.profile = '';
			}
			for (var i = 0, l = q.length; i < l; i++) {
				var filter = this.filterParse(q[i]);
				if (filter) {
					query[filter.type] = filter.value;
				}
			}
			var tq = [
				'keywords=' + query.keywords,
				'sortOrder=' + query.sortOrder,
				'distance=' + $spa.distance(query.miles),
				'itemFilter(0).name=MinPrice&itemFilter(0).value=' + query.minmax[0],
				'itemFilter(1).name=MaxPrice&itemFilter(1).value=' + query.minmax[1],
				'itemFilter(2).name=ListingType&itemFilter(2).value=' + query.ListingType,
				'itemFilter(3).name=Category&itemFilter(3).value=' + query.category,
				'itemFilter(4).name=Seller&itemFilter(4).value=' + query.profile,
				'itemFilter(5).name=Tag&itemFilter(5).value=' + query.tag
				];
			if (cursor) {
				tq.push('cursor=' + encodeURIComponent(cursor));
			}
			query.s = tq.join('&');
			query.hash = query.s.hashCode();
			if (cursor) {
				this.data[this.query.hash].next = query;
			}
			this.query = query;
			this.getResults();
		},
		filterParse: function (f) {
			var r = false;
			switch (true) {
				case /\d+-\d+/.test(f):
					r = {type: 'minmax', value: f.split('-')};
				break;
				case /Auction|FixedPrice/.test(f):
					r = {type: 'ListingType', value: f};
				break;
				case /category-(.*)/.test(f):
					r = {type: 'category', value: f.match(/category-(.*)/)[1]};
				break;
				case /\d+mi/.test(f):
					r = {type: 'miles', value: f.match(/(\d+)mi/)[1]};
				break;
			}
			return r;
		},
		getResults: function () {
			var self = this;
			if (typeof this.data[this.query.hash] === 'undefined') {
				$.getJSON('http://search.api.epek.com/query?' + this.query.s, function (r) {
					var ctx = {
						items: [],
						query: self.query,
						paging: {},
						maxPrice: Math.ceil(r.findItemsAdvancedResponse[0].searchResult[0].maxPrice)
					};
					var tp = r.findItemsAdvancedResponse[0].paginationOutput[0];
					for (page_item in tp) {
						ctx.paging[page_item] = tp[page_item][0];
					}
					var items = r.findItemsAdvancedResponse[0].searchResult[0].item;
					for (var i = 0, l = items.length; i < l; i++) {
						var product_url = items[i].sellerInfo[0].sellerUserName[0] + '/' + items[i].titleSlug[0];
						if (typeof $spa.controllers.product.data[product_url] === 'undefined') {
							$spa.controllers.product.data[product_url] = new Product(items[i]);
						} else {
							$spa.controllers.product.data[product_url].setData(items[i]);
						}
						ctx.items.push($spa.controllers.product.data[product_url]);
					}
					self.data[self.query.hash] = ctx;
					if (self.query.profile) {
						if (typeof $spa.controllers.profile.data[self.query.profile] === 'undefined') {
							$spa.controllers.profile.data[self.query.profile] = new Profile(self.query.profile, function () {
								self.show();
							}, true);
						} else {
							$spa.controllers.profile.data[self.query.profile].fetchFullProfile(function () {
								self.show();
							});
						}
					} else {
						self.show();
					}
				}).error(function (x, t) {
					$('#return').empty('').show();
					$spa.loading.hide();
					$('<p>' + t + '</p>').css({'text-align': 'center', 'font-size': '80px', 'color': '#369'}).appendTo('#return');
				});
			} else {
				this.show();
			}
		},
		show: function () {
			var self = this;
			var o = this.data[this.query.hash];
			if (this.query.cursor) {
				if (this.data[this.query.hash].paging.cursor == '') {
					$(window).unbind('.loader');
				}
				$('#search > ul').append(this.itemTpl(o)).removeClass('loading');
				self.sizes();
			} else {
				if (!$spa.router.custom) {
					$('#header form .real:text').val(this.query.keywords);
				}
				if (this.query.profile) {
					$spa.controllers.profile.init(this.query.profile);
				}
				if (this.query.tag) {
					$spa.controllers.tag.init(this.query.tag);
				}
				$('#search').html(this.tpl(o)).show();
				$('#search > ul').html(this.itemTpl(o));
				if (!$spa.router.custom) {
					$filter.updateMaxLimit(isNaN(o.maxPrice) ? 0 : o.maxPrice);
					$filter.setMinMax(this.query.minmax);
					$filter.setDistance(this.query.miles);
				}
				if (!this.query.tag) {
					$('#filter').show();
				}
				$spa.loading.hide();

				//scroll events to show more results
				if (this.data[this.query.hash].paging.cursor != '') {
					var $ul = $('#search > ul');
					if ($ul.length) {
						var wh, ultop, ulheight;
						self.sizes = function () {
							wh = $(window).height();
							ultop = $ul.offset().top;
							ulheight = $ul.outerHeight();
						}
						self.sizes();
						$(window).bind('scroll.loader', function () {
							if (!$ul.is('.loading')) {
								if ($(this).scrollTop() > ultop + ulheight - wh) {
									$ul.addClass('loading');
									self.parseQuery(self.query.original, null, self.data[self.query.hash].paging.cursor);
								}
							}
						});
						$(window).bind('resize.loader', function () {
							self.sizes();
						});
					}
				}
			}
			if (o.next) {
				this.parseQuery(this.query.original, null, this.data[this.query.hash].paging.cursor);
			} else if (o.query.scrollTop) {
				$(window).scrollTop(o.query.scrollTop);
			}
		},
		setupHandlers: function () {
			if (!this.hasHandlers) {
				var self = this;
				this.hasHandlers = true;
				$('#search').on('click', 'nav a', function (e) {
					e.preventDefault();
					$(this).parents('ul').data('listing-type', $(this).parent().data('listing-type'));
					if (self.query.profile) {
						$spa.navigate('profile', self.query.profile)
					} else {
						$('#header form').submit();
					}
				}).on('click', 'a', function () {
					self.query.scrollTop = $(window).scrollTop();
				});
				$('#profile').on('click', 'a', function (e) {
					if (!$(this).is('[href^="http"]')) {
						e.preventDefault();
					}
				}).on('click', 'a.edit_button', function () {
					$(this).parent().parent().removeClass('hoverable').find('.edit').addClass('active').find('input').focus();
				}).on('click', '.button.cancel', function () {
					$(this).parent().parent().removeClass('active');
					$('.seller_description').addClass('hoverable');
				}).on('click', '.button.confirm', function () {
					var input = $(this).parent().parent().find('input');
					var old = input.data('value');
					var text = $.trim(input.val());
					$(this).parent().parent().removeClass('active');
					$('.seller_description').addClass('hoverable');
					if (text != old) {
						input.data('value', text);
						var h = input.parent().parent().children('span').text(text);
						$.ajax({
							'url': 'http://upload-profile.epek.com/service/description?sessionId=' + $login.sess_id,
							'type': 'post',
							'contentType': 'application/json',
							'data': '{"message": "' + text + '"}',
							'dataType': 'json',
							'complete': function (xhr, statustext) {
								response = $.parseJSON(xhr.responseText);
								switch (statustext)  {
									case 'success':
										if (response.STATUS == 'OK') {
											$notification.show('success', 'Information updated succesfully', true);
											$spa.controllers.profile.data[self.query.profile].description = text;
											h.text($spa.controllers.profile.data[self.query.profile].descriptionText());
										} else {
											$notification.show('error', 'An error has occured (' + response.REASON + ')', true);
											h.text(old);
										}
									break;
									default:
										$notification.show('error', 'An error has occured (' + statustext + ')', true);
										h.text(old);
								}
							}
						});
					}

				});
			}
		}
	},
	product: {
		data: {},
		cache: {},
		init: function (url) {
			var self = this;
			this.url = url;
			this.setupHandlers();
			var parts = url.split('/');
			if (typeof this.tpl === 'undefined') {
				this.tpl = {
					'header': Handlebars.compile($('#product header script').html()),
					'bidding': Handlebars.compile($('#product .bidding script').html()),
					'gallery': Handlebars.compile($('#product .gallery-wrapper script').html()),
					'images': Handlebars.compile($('#product .images script').html()),
					'info': Handlebars.compile($('#product .info script').html()),
					'relationship': Handlebars.compile($('#product .relationship script').html())
				}
			}
			if (typeof this.data[url] === 'undefined' || !this.data[url].hasSingleItemData()) {
				$.getJSON('http://offer.epek.com/api/getSingleItemV2?Seller=' + parts[0] + '&TitleSlug=' + parts[1], function (r) {
					if (typeof self.data[url] === 'undefined') {
						self.data[url] = new Product(r.Item);
					} else {
						self.data[url].setData(r.Item);
					}
					var done = function () {
						self.data[url].setProfile($spa.controllers.profile.data[r.Item.Seller.UserID]);
						self.show(url);
						$spa.loading.hide();
					}
					$.getJSON('http://where.yahooapis.com/geocode?location=' + r.Item.Location + '&flags=J&gflags=R&locale=en_US', function (l) {
						if (l.ResultSet && l.ResultSet.Results) {
							self.data[url].setLocation(l.ResultSet.Results[0]);
						}
						if (typeof $spa.controllers.profile.data[r.Item.Seller.UserID] === 'undefined') {
							$spa.controllers.profile.data[r.Item.Seller.UserID] = new Profile(r.Item.Seller.UserID, function () {
								done();
							});
						} else {
							done();
						}
					});
				}).error(function (x) {
					$('#return').empty('').show();
					$spa.loading.hide();
					$('<p>' + x.status + '</p>').css({'text-align': 'center', 'font-size': '80px', 'color': '#369'}).appendTo('#return');
				});
			/*
				if (this.data[url] && this.data[url].hasSearchData) {
					this.show(url); //we have data from a previous search result. Let's show it while the rest of the data gets in.
				}
				*/
			} else {
				this.show(url);
				$spa.loading.hide();
			}
		},
		show: function (url) {
			var $h = $('#product');
			this.showItemRelationship(url);
			$h.find('header').html(this.tpl.header(this.data[url]));
			$h.find('.bidding').html(this.tpl.bidding(this.data[url]));
			$h.find('.gallery-wrapper').html(this.tpl.gallery(this.data[url]));
			$h.find('.images').html(this.tpl.images(this.data[url]));
			$h.find('.info').html(this.tpl.info(this.data[url]));
			$h.show();
			this.cache[$('.gallery-wrapper div img').attr('src')] = 1;
		},
		showItemRelationship: function (url) {
			var self = this;
			this.data[url].fetchRelationship(function () {
				$('#product .relationship').html(self.tpl.relationship(this));
				$('#product header').html(self.tpl.header(this));
			});
		},
		setupHandlers: function () {
			if (!this.hasHandlers) {
				this.hasHandlers = true;
				var self = this;
				$('#product').on('click', 'header a', function (e) {
					e.preventDefault();
					self.actions[$(this).parent().attr('class')].call(self, {e: e, actor: this});
				}).on('submit', 'form', function (e) {
					e.preventDefault();
					self.actions[$(this).attr('class')].call(self, {e: e, actor: $(this).find('button'), form: $(this)});
				}).on('click', '.bids a', function (e) {
					e.preventDefault();
					self.actions.bids.call(self);
				}).on('click', '.images img', function () {
					self.actions.changeImage.call(self, this);
				}).on('click', '.profile button', function (e) {
					self.actions.contact.call(self, {e: e, actor: $(this)});
				}).on('focus', 'form.bid input', function () {
					$(this).parent().find('span').fadeIn();
				}).on('blur', 'form.bid input', function () {
					$(this).parent().find('span').fadeOut();
				}).on('click', '.relationship button', function () {
					window.location.href = $(this).data('href');
				}).on('click', 'a.fs', function (e) {
					e.preventDefault();
					var w = $('.gallery-wrapper');
					if (screenfull) {
						screenfull.toggle(w.get(0));
					} else {
						windowfull.toggle(w);
						var ac = $('#product .images .active')
						if (!ac.is('.video')) {
							ac.click();
						}
					}
				}).on('click', '.gallery-wrapper li a', function (e) {
					e.preventDefault();
					var $this = $(this);
					var a = $('#product .images .active').parent();
					if ($this.is('.next')) {
						if (a.is(':last-child')) {
							a.parent().find(':first-child').find('img').click();
						} else {
							a.next().find('img').click();
						}
					} else if($this.is('.prev')) {
						if (a.is(':first-child')) {
							a.parent().find(':last-child').find('img').click();
						} else {
							a.prev().find('img').click();
						}
					}
				});
				if (screenfull) {
					screenfull.onchange = function() {
						var w = $('.gallery-wrapper');
						if (screenfull.isFullscreen) {
							w.addClass('fullscreen');
						} else {
							w.removeClass('fullscreen');
						}
						setTimeout(function () {
							var ac = $('#product .images .active')
							if (!ac.is('.video')) {
								ac.click();
							}
						}, 1000);
					};
				}
			}
		},
		actions: {
			share: function (options) {
				var self = this;
				$spa.needsLogin(options, function () {
					if (typeof self.tpl.share === 'undefined') {
						self.tpl.share = Handlebars.compile($('#share-tpl').html())
					}
					$overlay.show({html: self.tpl.share(self.data[self.url])});
					$social.check(function () {
						$overlay.update({html: self.tpl.share(self.data[self.url])});
					});
				});
			},
			watch: function (options) {
				$spa.needsLogin(options, function () {
					if (!$(options.actor).is('.loading')) {
						var opts = {
							'url': 'http://offer.epek.com/watch',
							'type': 'post',
							'dataType': 'json',
							'contentType': 'application/json',
							'complete': function () {
								$(options.actor).removeClass('loading');
								$spa.loading.hide();
							},
							'error': function (x, t, e) {
								//$product.notification('warning', 'An error has occured (' + (t ? t + ' ' : '') + (e ? e : '') + ')', true);
							}
						};
						var data = {
							'SessionId': $login.sess_id,
							'ItemID': $spa.controllers.product.data[$spa.controllers.product.url].itemId()
						}

						$(options.actor).addClass('loading');
						$spa.loading.show();

						if ($(options.actor).is('.Watching')) {
							data.Watch = false;
						} else {
							data.Watch = true;
						}
						opts.success = function () {
							$spa.controllers.product.data[$spa.controllers.product.url].setWatching(data.Watch);
							$('#product header').html($spa.controllers.product.tpl.header($spa.controllers.product.data[$spa.controllers.product.url]));
						}
						opts.data = JSON.stringify(data);
						$.ajax(opts);
					}
				});
			},
			report: function (options) {
				var self = this;
				$spa.needsLogin(options, function () {
					if (typeof self.tpl.report === 'undefined') {
						self.tpl.report = Handlebars.compile($('#report-tpl').html())
					}
					$overlay.show({html: self.tpl.report(self.data[self.url])});
				});
			},
			bid: function (options) {
				var self = this;
				var v = parseFloat(options.form.find(':text').val());
				if (v && !isNaN(v)) {
					$spa.needsLogin(options, function () {
						if (typeof self.tpl.bid === 'undefined') {
							self.tpl.bid = Handlebars.compile($('#bid-tpl').html())
						}
						self.data[self.url].temptativeBid(v);
						$overlay.show({html: self.tpl.bid(self.data[self.url])});
					});
				}
			},
			bin: function (options) {
				var self = this;
				$spa.needsLogin(options, function () {
					if (typeof self.tpl.bin === 'undefined') {
						self.tpl.bin = Handlebars.compile($('#bin-tpl').html())
					}
					$overlay.show({html: self.tpl.bin(self.data[self.url])});
				});
			},
			bids: function () {
				if (typeof this.tpl.bids === 'undefined') {
					this.tpl.bids = Handlebars.compile($('#bids-tpl').html())
				}
				$overlay.show({html: this.tpl.bids(this.data[this.url])});
				var self = this;
				this.data[this.url].fetchBids(function () {
					$overlay.update({html: self.tpl.bids(self.data[self.url])});
				});
			},
			changeImage: function (img) {
				var $img = $(img);
				var li = $img.parent();
				var ul = li.parent();
				var cnt = ul.parent();
				var wrapper = $('#product .gallery-wrapper');
				var old = wrapper.find('div');
				if ($img.is('.video')) {
					wrapper.addClass('video');
					var d = $('<div><iframe src="http://www.youtube.com/embed/' + $img.data('videoid') + '?wmode=opaque&modestbranding=1&autohide=1&color=red&hd=1&rel=0&showinfo=0&theme=light" width="100%" height="100%"></iframe></div>').hide().appendTo(wrapper).fadeIn(function () {
						old.remove();
					});
				} else {
					wrapper.removeClass('video');
					if (wrapper.is('.fullscreen')) {
						var big = $(img).data('huge');
					} else {
						var big = $(img).data('big');
					}
					if (!(big in this.cache)) {
						this.cache[big] = 1;
						old.fadeOut();
					}

					var i = $(new Image()).load(function () {
						var d = $('<div></div>').hide().appendTo(wrapper);
						d.append(this).fadeIn(function () {
							old.remove();
						});
					}).attr('height', wrapper.height()).attr('src', big);
				}

				if (li.is(':first-child')) {
					ul.css('margin-left', 0);
				} else if (li.is(':last-child')) {
					ul.css('margin-left', -(li.position().left) + cnt.outerWidth() - li.outerWidth());
				} else {
					ul.css('margin-left', -(li.position().left) + (cnt.outerWidth() / 2) - (li.outerWidth() / 2));
				}
				ul.find('.active').removeClass('active').attr('height', 88);
				$img.addClass('active').attr('height', 100);
			},
			contact: function (options) {

				var self = this;
				$spa.needsLogin(options, function () {
					if (typeof self.tpl.contact === 'undefined') {
						self.tpl.contact = Handlebars.compile($('#contact-tpl').html())
					}
					$overlay.show({html: self.tpl.contact(self.data[self.url])});
				});
			},
			placebid: function (form) {
				var $form = $(form);
				$form.find('> p').removeClass('error').find('span').text('');
				var parsedBid = function () {
					var st = $form.find('input[name=bid]').val();
					var pos = st.indexOf('.');
					if (pos == -1) {
						return parseInt(st, 10) * 100;
					} else if (st.length - pos == 3) {
						return parseInt(st.replace('.', ''), 10);
					} else if (st.length - pos == 2) {
						return parseInt(st.replace('.', ''), 10) * 10;
					} else if (st.length - pos > 3) {
						return parseInt(st.substr(0, pos + 3).replace('.', ''), 10);
					}
				};
				var o = {
					'SessionId': $login.sess_id,
					'ItemID': parseInt($(form).find('input[name=itemid]').val(), 10),
					'Offer': {
						'Action': 'Bid',
						'MaxBid': parsedBid(),
						'Quantity': 1,
						'UserConsent': true
					}
				}
				this.actions.placeOffer(o, {
					form: $form,
					success: function (r) {
						$spa.controllers.product.data[$form.data('url')].updateWithBidReturn(r);
						$('#product .bidding').html($spa.controllers.product.tpl.bidding($spa.controllers.product.data[$form.data('url')]));
						$overlay.send();
						$spa.controllers.product.showItemRelationship($form.data('url'));
					},
					error: function (s) {
						$form.find('> p').addClass('error').find('span').text(s);
					}
				});
			},
			buynow: function (form) {
				var $form = $(form);
				$form.find('> p').removeClass('error').find('span').text('');
				var parsedBid = function () {
					var st = $form.find('input[name=price]').val();
					var pos = st.indexOf('.');
					if (pos == -1) {
						return parseInt(st, 10) * 100;
					} else if (st.length - pos == 3) {
						return parseInt(st.replace('.', ''), 10);
					} else if (st.length - pos == 2) {
						return parseInt(st.replace('.', ''), 10) * 10;
					} else if (st.length - pos > 3) {
						return parseInt(st.substr(0, pos + 3).replace('.', ''), 10);
					}
				};
				var o = {
					'SessionId': $login.sess_id,
					'ItemID': parseInt($(form).find('input[name=itemid]').val(), 10),
					'Offer': {
						'Action': 'Purchase',
						'MaxBid': parsedBid(),
						'Quantity': parseInt($(form).find('select').val(), 10) || 1,
						'UserConsent': true
					}
				}
				this.actions.placeOffer(o, {
					form: $form,
					success: function () {
						location.href = 'http://stack.epek.com/checkout/?id=' + o.ItemID;
					},
					error: function (s) {
						$form.find('> p').addClass('error').find('span').text(s);
					}
				});
			},
			sendmessage: function (form) {
				var $form = $(form);
				if ($form.is('.loading')) {
					return;
				}
				$form.addClass('loading');
				$form.find('> p').removeClass('error').find('span').text('');
				$.ajax({
					'url': 'http://messages.api.epek.com/m2m/api/contact-seller?callback=?',
					'type': 'get',
					'dataType': 'jsonp',
					'data': $form.serialize(),
					'complete': function () {
						$form.removeClass('loading');
					},
					'success': function (r) {
						if (r && !r.errorMessage) {
							$overlay.send();
						} else {
							$form.find('> p').addClass('error').find('span').text(r.errorMessage ? r.errorMessage : 'Error');
						}
					},
					'error': function (x, t, e) {
						$form.find('> p').addClass('error').find('span').text('An error has occured (' + (t ? t + ' ' : '') + (e ? e : '') + ')');
					}
				});
			},
			sendreport: function (form) {
				var $form = $(form);
				if ($form.is('.loading')) {
					return;
				}
				$form.addClass('loading');
				$form.find('> p').removeClass('error').find('span').text('');
				$.ajax({
					'url': 'http://messages.api.epek.com/m2m/api/contact-cust-service?callback=?',
					'type': 'post',
					'dataType': 'json',
					'data': {'content': $form.find('textarea').val() + '\n' + window.location.href, 'userId': $login.username},
					'success': function (r) {
						if (r && r.result) {
							$overlay.send();
						} else {
							$form.find('> p').addClass('error').find('span').text(r && r.errorMessage ? r.errorMessage : 'Error');
						}
					},
					'error': function (x, r, s) {
						$form.find('> p').addClass('error').find('span').text(r + (s ? ' ' + s : ''));
					},
					'complete': function (r) {
						$form.removeClass('loading');
					}
				});
			},
			placeOffer: function (offer, options) {
				if (options.form.is('.loading')) {
					return;
				}
				options.form.addClass('loading');
				var sdata = JSON.stringify(offer);
				$.ajax({
					'url': 'http://offer.epek.com/api/placeOffer',
					'type': 'post',
					'dataType': 'json',
					'contentType': 'application/json',
					'data': sdata,
					'complete': function (r) {
						options.form.removeClass('loading');
					},
					'success': function (r) {
						if (r.Error) {
							options.error(r.Error.LongMessage ? r.Error.LongMessage : 'Error');
						} else {
							options.success(r);
						}
					},
					'error': function (x, t, e) {
						options.error('An error has occured (' + (t ? t + ' ' : '') + (e ? e : '') + ')');
					}
				});
			},
			postsocial: function (form) {
				var $form = $(form);
				$form.find('> p').removeClass('error').find('span').text('');
				var services = [];
				var fb_a = $form.find('a.facebook');
				if (fb_a.is('.selected')) {
					services.push('facebook');
				}
				var tw_a = $form.find('a.twitter');
				if (tw_a.is('.selected')) {
					services.push('twitter');
				}
				if (services.length == 0) {
					$form.find('> p').addClass('error').find('span').text('You must select at least 1 service.');
				} else {
					$form.addClass('loading');
					$social.post({
						services: services,
						title: $form.find('input[name=title]').val(),
						message: $form.find('textarea').val(),
						image: $form.find('input[name=image]').val(),
						url: $form.find('input[name=url]').val()
					}, function () {
						$form.removeClass('loading');
						$overlay.send();
					});
				}
			}
		}
	},
	profile: {
		data: {},
		init: function (id) {
			this.setupHandlers();
			this.profile = this.data[id];
			if (typeof this.profileTpl === 'undefined') {
				this.profileTpl = Handlebars.compile($('#profile script').html());
				this.feedbackTpl = Handlebars.compile($('#feedback-tpl').html());
				this.followingTpl = Handlebars.compile($('#following-tpl').html());
				this.followersTpl = Handlebars.compile($('#followers-tpl').html());
			}
			$('#profile').html(this.profileTpl(this.data[id])).show();
			var uploaders = $('#profile input[type=file]');
			uploaders.each(function () {
				var $this = $(this);
				var $p = $this.parents('div.uploader');
				var t = $p.data('type');
				$this.fileupload({
					'dataType': 'json',
					'url': 'http://upload-profile.epek.com/service/upload?type=' + t + '&sessionId=' + $login.sess_id,
					'start': function (e, data) {
						$notification.show('info', 'Uploading image...', false);
					},
					'progress': function (e, data) {
						$notification.show('info', 'Uploading image (' + parseInt(data.loaded / data.total * 100, 10) + '%) ...', false);
					},
					'done': function (e, data) {
						$notification.show('success', 'Image upladed', true);
						var img = $p.siblings('img');
						var old_src = img.attr('src').split('?')[0];
						img.attr('src', old_src + '?' + data.result[0].size)
					},
					'fail': function (e, data) {
						$notification.show('error', 'An error has occured', true);
					}
				});
			})
		},
		setupHandlers: function () {
			if (!this.hasHandlers) {
				this.hasHandlers = true;
				var self = this;
				$('#profile').on('click', 'a.feedback, a.transactions', function () {
					self.showFeedback();
				}).on('click', 'a.follow.fbutton', function () {
					var $this = $(this);
					$spa.needsLogin({actor: this}, function () {
						if ($this.is('.loading')) {
							return;
						}
						$this.addClass('loading');
						if ($this.is('.active')) {
							var action = 'unfollow';
						} else {
							var action = 'follow';
						}
						$.ajax({
							url: 'http://offer.epek.com/api/' + action,
							data: '{"SessionId": "' + $login.sess_id + '", "username": "' + self.profile.id + '"}',
							dataType: 'json',
							type: 'post',
							contentType: 'application/json',
							success: function () {
								if (action == 'follow') {
									$this.addClass('active');
								} else {
									$this.removeClass('active');
								}
							},
							complete: function () {
								$this.removeClass('loading');
							}
						});
					});
				}).on('click', 'a.following.fbutton', function () {
					self.showFollowing();
				}).on('click', 'a.followers.fbutton', function () {
					self.showFollowers();
				});
			}
		},
		showFeedback: function () {
			var self = this;
			$overlay.show({html: this.feedbackTpl(this.profile)});
			this.profile.fetchFeedbackItems(function () {
				$overlay.update({html: self.feedbackTpl(self.profile)});
			});
		},
		showFollowing: function () {
			var self = this;
			$overlay.show({html: this.followingTpl(this.profile)});
			this.profile.fetchFollowingItems(function () {
				$overlay.update({html: self.followingTpl(self.profile)});
			});
		},
		showFollowers: function () {
			var self = this;
			$overlay.show({html: this.followersTpl(this.profile)});
			this.profile.fetchFollowersItems(function () {
				$overlay.update({html: self.followersTpl(self.profile)});
			});
		}
	},
	tags: {
		data: {},
		init: function () {
			var self = this;
			if ('tags' in this.data) {
				this.show();
			} else {
				this.tpl = Handlebars.compile($('#tags-tpl').html());
				$.getJSON('http://search.api.epek.com/tags', function (r) {
					self.data = r;
					self.show();
				});
			}
		},
		show: function () {
			$('#tags').html(this.tpl({tags: this.data.tags})).show();
			$spa.loading.hide();
		}
	},
	tag: {
		data: {},
		init: function (tag) {
			if (typeof this.tpl === 'undefined') {
				this.tpl = Handlebars.compile($('#tag script').html());
			}
			$('#tag').html(this.tpl({tag: tag})).show();
		}
	}
}
