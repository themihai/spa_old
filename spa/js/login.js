$login = {
	'init': function (r) {
		if (!this.done) {
			this.done = true;
			this.session = r;
			if (r.sessionid == "invalid") {
				this.username = null;
				this.loggedIn(false);
			} else {
				this.username = r.user;
				this.sess_id = (result = new RegExp('(?:^|; )' + encodeURIComponent('sess_id') + '=([^;]*)').exec(document.cookie)) ? result[1] : null;
				this.loggedIn(true);
			}
			$spa.init();
		}
	},
	'loggedIn': function (isLogged) {
		if (isLogged) {
			$('#header nav ul').toggleClass('logged-in logged-out').find('a.user').text(this.username);
		}
	}
};
$(function () {
	$('<script></script>').attr('src', 'https://signin.epek.com/api/checksession?callback=$login.init').appendTo('body');
	setTimeout(function () {
		$login.init({sessionid: 'invalid'});
		//$login.init({user: 'marcos'});
	}, 1500)
});
