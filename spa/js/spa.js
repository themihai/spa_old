$spa = {
	tpl: {},
	init: function () {
		var self = this;
		this.loading = {
			hide: function () {
				$('#wrapper').removeClass('loading');
			},
			show: function () {
				$('#wrapper').addClass('loading');
			}
		};
		this.sections = $('body > section, #wrapper > section');

		var location_cb = function (city) {
			self.getLocation(city);
			self.router.setup();
			self.setupHeader();
		}
		if ($.cookie('city')) {
			location_cb(JSON.parse($.cookie('city')));
		} else {
			$.getJSON('http://util.epek.com/location?callback=?', function (r) {
				var city = {lat: r.ipinfo.Location.latitude, lon: r.ipinfo.Location.longitude, name: r.ipinfo.Location.CityData.city};
				location_cb(city);
				$.cookie('city', JSON.stringify(city));
			});
		}

	},
	getLocation: function (city) {
		this.city = city;
		this.showGeolocator();
		this.router.refresh();
	},
	router: {
		setup: function () {
			/* 
			 * simple router
			 * regexp to match and controller to call, plus extra param to the init
			 * the captured string will be passed as first param to the controller
			 */
			this.routes = [
				[/^home$/, 'home'],
				[/^search\/(.*)/, 'search'],
				[/^tag[\/]?$/, 'tags'],
				[/^tag\/(.*)/, 'search', 'tag'],
				[/^(.+\/(Auction|FixedPrice).*)$/, 'search', 'profile'],
				[/^([\w-]+\/[a-zA-Z][\w-_]+)$/, 'product'],
				[/^(.+)$/, 'search', 'profile']
			];
			/* listen to hash change events. TODO: fix old IE's and others, maybe with jquery hashchange plugin */
			$(window).bind('hashchange', function () {
				$spa.router.init();
			});
			this.init();
		},
		init: function (custom) {
			this.custom = custom;
			$(window).scrollTop(0);
			$overlay.close();
			$(window).unbind('.loader');
			$spa.sections.hide();
			if ($('#product .relationship script').length == 0) {
				$('#product .relationship').empty();
			}
			$spa.loading.show();
			$('#wrapper').removeClass('filter');
			var r = custom || window.location.hash.replace('#!/', '');
			if (r == '') {
				window.location.replace('#!/home');
			} else {
				for (var i = 0, l = this.routes.length; i < l; i++) {
					var m = r.match(this.routes[i][0]);
					if (m) {
						$('body').attr('id', (this.routes[i][2] || this.routes[i][1]) + '-page');
						$spa.controllers[this.routes[i][1]].init(m[1], this.routes[i][2]);
						break;
					}
				}
				if (!m) {
					$('#return').empty('').show();
					$spa.loading.hide();
					$('<p>c404</p>').css({'text-align': 'center', 'font-size': '80px', 'color': '#369'}).appendTo('#return');
				}
			}
			this.ran = true;
		},
		refresh: function () {
			if (this.ran) {
				$spa.controllers.home.done = false;
				this.init(this.custom);
			}
		}
	},
	setupHeader: function () {
		var self = this;
		var $f = $('#header form');
		$f.submit(function (e) {
			$spa.navigate('search', $(this).find('.real:text').val(), true);
			$f.find('.real:text').blur();
			return false;
		}).find('.real:text').autocomplete({
			source: function( request, response ) {
				$.ajax({
					url: "http://suggestqueries.google.com/complete/search",
					dataType: "jsonp",
					data: {
						client: "products",
						q: request.term
					},
					jsonp: 'jsonp',
					success: function( data ) {
						response( $.map( data[1].slice(0,4), function( item ) {
							return {
								label: item[0],
								value: item[0]
							}
						}));
					}
				});
			},
			appendTo: $f.find('> div'),
			minLength: 2,
			select: function( event, ui ) {
				if (ui.item) {
					$spa.navigate('search', ui.item.value, true);
				}
			},
			delay: 0
		});
		$('#header').on('click', '.geo li a', function (e) {
			e.preventDefault();
			var $this = $(this);
			var c = self.nearbyCities[$(this).parent().index()];
			var city = {lat: c.lat, lon: c.lng, name: c.name};
			$.cookie('city', JSON.stringify(city));
			self.getLocation(city);
		}).on('click', '.geo > div a', function (e) {
			e.preventDefault();
			var $clicker = $(this).parent();
			$overlay.show({
				html: self.tpl.citysearch({cities: false}),
				pos: $clicker.offset(),
				cities: true,
				onclick: function (e) {
					var $this = $(this);
					var city = $this.parent().data('city');
					$.cookie('city', JSON.stringify(city));
					self.getLocation(city);
					$overlay.close();
				},
				onshow: function () {
					/* google autocomplete for cities */
					var ac_options = {
						types: ['(cities)'],
						componentRestrictions: {country: 'us'}
					}
					street1_ac = new google.maps.places.Autocomplete($(this).find(':text').get(0), ac_options);
					google.maps.event.addListener(street1_ac, 'place_changed', function() {
						var place = street1_ac.getPlace();
						var city = {lat: place.geometry.location.lat(), lon: place.geometry.location.lng(), name: place.name};
						$.cookie('city', JSON.stringify(city));
						self.getLocation(city);
						$overlay.close();
					});
				}
			});
			var distance = function (lat2, lon2) {
				var lat1 = self.city.lat;
				var lon1 = self.city.lon;
				var radlat1 = Math.PI * lat1/180;
				var radlat2 = Math.PI * lat2/180;
				var radlon1 = Math.PI * lon1/180;
				var radlon2 = Math.PI * lon2/180;
				var theta = lon1-lon2;
				var radtheta = Math.PI * theta/180;
				var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);;
				dist = Math.acos(dist);
				dist = dist * 180/Math.PI;
				dist = dist * 60 * 1.1515;
				//dist = dist * 1.609344;
				return dist;
			}
			var sortDist = function (a, b) {
				return a[10] - b[10];
			}
			var roundDistance = function (distance) {
				return distance >= 10 ? Math.ceil(parseInt(distance, 10) / 10) * 10 : distance;
			}
			var cities_near = [];
			var cities_farther = [];
			var parse = function () {
				var cities = self.US;

				for (var i = 0;i < cities.length;i++) {
					var city = cities[i];
					cities[i][10] = distance(city[4], city[5]);
				}

				cities.sort(sortDist);
				for (var i = 0, l = cities.length; i < l; i++) {
					if (cities_near.length <= 4) {
						if (cities[i][10] > 1) {
							cities_near.push({name: cities[i][1], distance: Math.round(cities[i][10]), lat: cities[i][4], lon: cities[i][5], name: cities[i][1]});
						}
					} else if (cities_farther.length <= 8) {
							cities_farther.push({name: cities[i][1], distance: Math.round(cities[i][10]), lat: cities[i][4], lon: cities[i][5], name: cities[i][1]});
					} else {
						break;
					}
				}
				$overlay.update({
					html: self.tpl.citysearch({
						list: [
							{distance: roundDistance(cities_near[cities_near.length - 1].distance), cities: cities_near},
							{distance: roundDistance(cities_farther[cities_farther.length - 1].distance), cities: cities_farther}
						]
					})
				});
			}
			if (self.US) {
				parse();
			} else {
				$.getJSON('http://store.min.epek.com/data/US.json', function (r) {
					self.US = r;
					parse()
				});
			}
		});
	},
	showGeolocator: function () {
		var self = this;
		if (!this.tpl.geolocator) {
			this.tpl.geolocator = Handlebars.compile($('#geolocator-tpl').html());
			this.tpl.citysearch = Handlebars.compile($('#citysearch-tpl').html());
		} else {
			$('#header .geo > div').empty().addClass('loading');
		}
		var lat = this.city.lat;
		var lon = this.city.lon;
		$.getJSON('http://api.geonames.org/citiesJSON?north=' + (lat - 1) + '&south=' + (lat + 1) + '&east=' + (lon + 1) + '&west=' + (lon - 1) + '&username=epek', function (t) {
			self.nearbyCities = t.geonames;
			var change = false;
			for (var i = 0, l = self.nearbyCities.length; i < l; i++) {
				if (self.nearbyCities[i].name == self.city.name) {
					change = i;
				}
			}
			if (change !== false) {
				self.nearbyCities.splice(change, 1);
			}
			$('#header .geo').html(self.tpl.geolocator({cities: self.nearbyCities, city: self.city}));
		});
		
	},
	navigate: function (type, s, reset) {
		var mm = $filter.getMinMax();
		var d = $filter.getDistance();
		var lt = $('#search nav ul').data('listing-type');
		switch (type) {
			case 'search':
				if (reset) {
					window.location.href = '#!/search/' + s;
				} else {
					window.location.href = '#!/search/' + s + (this.controllers.search.query && this.controllers.search.query.category ? '/category-' + this.controllers.search.query.category : '') + (mm ? '/' + mm[0] + '-' + mm[1] : '') + (lt && lt !== 'All' ? '/' + lt : '') + (d ? '/' + d + 'mi' : '');
				}
			break;
			case 'tag':
				window.location.href = '#!/tag/' + s;
			break;
			case 'profile':
				window.location.href = '#!/' + s + (mm ? '/' + mm[0] + '-' + mm[1] : '') + (lt !== 'All' ? '/' + lt : '') + (d ? '/' + d + 'mi' : '');
			break;
		}
	},
	needsLogin: function (options, cb) {
		if ($login.session.sessionid == 'invalid') {
			if (typeof this.tpl.login === 'undefined') {
				this.tpl.login = Handlebars.compile($('#login-tpl').html())
			}
			var pos = $(options.actor).offset();
			pos.top += $(options.actor).outerHeight();
			var wr = $('section > .wrapper').eq(0);
			var right = false;
			var left = false;
			if (wr.length) {
				var wr_pos = wr.offset();
				var wr_width = wr.outerWidth();
				if (pos.left + 300 > wr_pos.left + wr_width) {
					right = true;
				}
				if (pos.left < wr_pos.left + 50) {
					left = true;
				}
			}
			$overlay.show({html: this.tpl.login(), pos: pos, right: right, left: left}, function () {
				$(this).find('button').click(function () {
					window.location.href = 'http://signin.epek.com/login?serviceurl=' + encodeURIComponent(window.location.href);
				});
			});
		} else {
			cb();
		}
	},
	distance: function (miles) {
		return [this.city.lat, this.city.lon, parseInt(miles * 1609.344, 10) || 12000000].join(',');
	}
}

Handlebars.registerHelper('lowercase', function(s) {
	return s.toLowerCase();
});
String.prototype.hashCode = function(){
	var hash = 0;
	if (this.length == 0) return hash;
	for (i = 0; i < this.length; i++) {
		chr = this.charCodeAt(i);
		hash = ((hash<<5)-hash)+chr;
		hash = hash & hash; // Convert to 32bit integer
	}
	return hash;
}
