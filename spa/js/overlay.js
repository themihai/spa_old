$overlay = {
	setup: function () {
		var self = this;
		var overlay = $('<div id="overlay"></div>').hide().appendTo('body');
		var bg = $('<div class="bg"></div>').appendTo(overlay);
		var cnt = $('<div class="cnt"><div></div></div>').hide().appendTo(overlay);
		var close = $('<a href="#" class="close icon-remove"></a>').click(function (e) {
			e.preventDefault();
			self.close();
		}).appendTo(cnt);
		overlay.on('click', '.cnt', function (e) {
			e.stopPropagation();
		});
		cnt.on('submit', 'form', function (e) {
			var ac = $(this).data('action').split('-');
			$spa.controllers[ac[0]].actions[ac[1]].call($spa.controllers[ac[0]], this);
			return false;
		}).on('click', 'button[type=reset]', function (e) {
			self.close();
		}).on('click', 'a', function (e) {
			e.preventDefault();
		}).on('click', 'a:not(.close)', function (e) {
			if (self.options && self.options.onclick) {
				self.options.onclick.call(this, e);
			}
		}).on('click', 'a.twitter', function (e) {
			var $this = $(this);
			if ($this.is('.selected')) {
				$this.removeClass('selected');
			} else {
				$social.connect('twitter', function () {
					$this.addClass('selected')
				});
			}
		}).on('click', 'a.facebook', function (e) {
			var $this = $(this);
			if ($this.is('.selected')) {
				$this.removeClass('selected');
			} else {
				$social.connect('facebook', function () {
					$this.addClass('selected')
				});
			}
		}).on('click', 'a.eclose', function (e) {
			var $this = $(this);
			$this.parent().removeClass('error');
		}).on('click', 'ul.tabs a', function (e) {
			var $this = $(this);
			cnt.find('a.selected').removeClass('selected');
			$this.addClass('selected');
			cnt.find('div.selected').removeClass('selected');
			cnt.find('div.' + $this.parent().attr('class')).addClass('selected');
		}).on('click', 'div.selected a', function (e) {
			window.location.href = $(this).attr('href');
		}).on('click', '.followers a, .following a', function (e) {
			window.location.href = $(this).attr('href');
		});
		this.update = function (options, cb) {
			cnt.find('> div').html(options.html);
			var cb = cb || function () {};
			cb.call(cnt);
		}
		this.show = function (options, cb) {
			self.options = options;
			cnt.find('> div').html(options.html);
			var cb = cb || function () {};
			cb.call(cnt);
			if (options.pos) {
				bg.css({opacity: 0.22});
				cnt.removeClass('fixed').css(options.pos).css({'bottom': 'auto', 'margin-bottom': 'auto'}).fadeIn();
				if (options.right) {
					cnt.addClass('right');
				} else {
					cnt.removeClass('right');
				}
				if (options.left) {
					cnt.addClass('left');
				} else {
					cnt.removeClass('left');
				}
				if (options.cities) {
					cnt.addClass('cities');
				} else {
					cnt.removeClass('cities');
				}
			} else {
				bg.css({opacity: 0.5});
				var h = $(window).outerHeight();
				cnt.addClass('fixed').css({
					'top': h,
					'bottom': 'auto',
					'max-height': h - 50
				}).show();
				setTimeout(function () {
					cnt.css('left', ($(window).outerWidth() - $('#overlay .cnt').outerWidth()) / 2).animate({'top': h - cnt.outerHeight()}, 300, function () {
						cnt.css({
							'top': 'auto',
							'bottom': 0
						});
					});
				}, 200)
			}
			overlay.fadeTo(200, 1, function () {
				$('body').bind('click.overlay', function () {
					$overlay.close();
				});
				if (self.options && self.options.onshow) {
					self.options.onshow.call(cnt);
				}
			});
		},
		this.close = function () {
			if (cnt.find('.loading').length) {
				return;
			}
			if (cnt.is('.fixed')) {
				cnt.animate({'bottom': -($(window).outerHeight())}, 300, 'easeInQuart', function () {
					overlay.animate({'opacity': 0}, 200, function () {
						overlay.hide();
						cnt.hide().css('margin-bottom', 0).find('> div').html('');
						$('body').unbind('click.overlay');
					});
				});
			} else {
				overlay.animate({'opacity': 0}, 400, function () {
					overlay.hide();
					cnt.hide().css('margin-bottom', 0).find('> div').html('');
					$('body').unbind('click.overlay');
				});
			}
		}
		this.send = function () {
			cnt.animate({'margin-bottom': $(window).outerHeight()}, 300, 'easeInQuart', function () {
				overlay.animate({'opacity': 0}, 200, function () {
					overlay.hide();
					cnt.hide().css('margin-bottom', 0).find('> div').html('');
					$('body').unbind('click.overlay');
				});
			});
		}
	}
}
$(function () {
	$overlay.setup();
})
