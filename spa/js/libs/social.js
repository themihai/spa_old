document.domain="epek.com"; // To avoid same origin issues
$social = {
	data: {},
	appurl: 'http://social.epek.com',
	check: function (cb) {
		var self = this;
		var _cb = cb || function () {};
		$.getJSON(this.appurl + '/widget?action=checkSocialStatus&callback=?', function (r) {
			for (item in r) {
				self.data[item] = r[item];
			}
			_cb();
		});
	},
	has: function (service) {
		return this.data[service + 'Auth'];
	},
	post: function (options, cb) {
		var desc = $social._getOGProperty("og:image");
		desc.attr("content", options.image);
		var desc = $social._getOGProperty("og:description");
		desc.attr("content", options.message);
		$.getJSON(this.appurl + '/post?callback=?', {
			service: options.services.join('+'),
			message: [options.message, options.title, options.url, '#epek'].join(' '),
			image: encodeURIComponent(options.image)
		}, function () {
			cb();
		});
	},
	connect: function (service, cb) {
		var self = this;
		var cb = cb || function () {};
		var datas = {
			'twitter': {
				service: 'twitter',
			},
			'facebook': {
				service: 'facebook',
				token: this.data.facebookToken
			}
		}
		if (this.data[service + 'Auth'] == false && this.data[service + 'AuthURL']) {
			switch (service) {
				case 'twitter':
					SocialShareWidget.twitterAuthResponse = function (r, token, url) {
						if (r == 'SUCCESS') {
							self.data.twitterAuth = true;
							self.data.twitterAuthURL = url;
							self.data.twitterToken = token;
							self.connect('twitter');
							cb();
						}
					}
					window.open(this.data.twitterAuthURL, "epek_share_twitterauth", "location=0,status=0,width=850,height=650");
					break;
				case 'facebook':
					SocialShareWidget.facebookAuthResponse = function (r, token, url) {
						if (r == 'SUCCESS') {
							self.data.facebookAuth = true;
							self.data.facebookAuthURL = url;
							self.data.facebookToken = token;
							self.connect('facebook');
							cb();
						}
					}
					window.open(this.data.facebookAuthURL, "epek_share_fbauth", "location=0,status=0,width=900,height=650");
					break;
			}
		} else {
			$.getJSON(this.appurl + '/connect?callback=?', datas[service], function (r) {
				if (typeof r.authenticated !== 'undefined') {
					self.data[service + 'Auth'] = r.authenticated;
					self.data[service + 'AuthURL'] = r.authorizationURL;
					cb();
				}
			});
		}
	},
	_getOGProperty : function (name) {

		var ogDesc = $("meta[property='" + name + "']");

		if (ogDesc.length == 0) {
			$("head").append('<meta property="' + name + '"  content="" />');
			ogDesc = $("meta[property='" + name + "']");
		}
		
		return ogDesc;
	}
}
//legacy object for calls, from services auth pages
SocialShareWidget = {};
