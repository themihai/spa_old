$filter = {
	getDistance: function () {
		var v = $('#radius').slider('value');
		var max = $('#radius').slider('option', 'max');
		if (v == max) {
			return false
		} else {
			return v;
		}
	},
	setDistance: function (m) {
		$('#radius').slider('option', 'value', m || $('#radius').slider('option', 'max'));
	},
	getMinMax: function () {
		var v = $('#min_max').slider('values');
		var min = $('#min_max').slider('option', 'min');
		var max = $('#min_max').slider('option', 'max');
		if (v[0] == min && v[1] == max) {
			return false
		} else {
			return v;
		}
	},
	setMinMax: function (d) {
		if (d[0] == '') {
			var min = $('#min_max').slider('option', 'min');
			var max = $('#min_max').slider('option', 'max');
			$('#min_max').slider('values', [min, max]);
		} else {
			$('#min_max').slider('option', 'values', d);
		}
	},
	updateMaxLimit: function (max) {
		var mm = $('#min_max');
		var values = mm.slider('values');
		var limit = mm.slider('option', 'max');
		mm.slider('option', 'max', max);
		if (values[1] == limit) {
			mm.slider('option', 'values', [values[0], max]);
		} else {
			mm.slider('option', 'values', values);
		}
	}
}
