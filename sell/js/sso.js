// SSO Validaton
			var sso_username = null;

			$.ajax({
	 			url : "https://signin.epek.com/api/checksession",
				type : "GET",
	 			cache : false,
				dataType : "jsonp",
				crossDomain : true,
	 			success : function (response) {

	 				if (response.sessionid == "invalid") {
	 					//Do not Redirect but store myepek
							$(".loggedout").html('<a href="http://stack.epek.com/activity/" title="">My <span class="e">E</span>pek</a>');
      //redirect until we fix the SSO app to forward the redirect URL after sign up 
	 			window.location.href = "https://signin.epek.com/login?serviceurl=" + encodeURIComponent(window.location.href);	
	 				}
					if (response.sessionid !== "invalid") {
	 				// Store Username
	 				sso_username = response.user;
	 				$(".loggedout").html('<a href="http://stack.epek.com/activity/" title="My Epek" class="logged">');
 					$(".logged").text(sso_username);
	                $(".last").html('</a><a href="http://signin.epek.com/logout" title="">(log out)</a>');
				}
				
	 			},
	 			error : function () {
	 				// Failed to validate. Force login
	 				window.location.href = "https://signin.epek.com/login?serviceurl=" + encodeURIComponent(window.location.href);
					sso_username = response.user;
	 				$(".loggedout").html('<a href="http://stack.epek.com/activity/" title="My Epek" class="logged">');
 					$(".logged").text(sso_username);
	                $(".last").html('</a><a href="http://signin.epek.com/logout" title="">(log out)</a>');
	
	 			}
	 		});
