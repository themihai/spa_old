function trim(e) {
    return e.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
}

(function($){
	$('#searchMessage input.text').click(function() {
		$(this).val('');
	});
	$('article.product input.text').click(function() {
		$(this).val('');
	});
	
	/* setup .inputdefault fields to have their helper text go away... */
    $('.inputdefault').live("focus",function(srcc) {
        if ($(this).val() == $(this)[0].title) {
            $(this).val('');
            if($(this).hasClass('password')) {
                try { 
                    // in case an old browser like ie8 or ie7 doesn't like this command
                    $(this).prop('type', 'password');
                } catch(err) {
                    $(this).replaceWith('<input type="password" value="" class="inputdefault password" id="password" style="font-family: Verdana, sans-serif;">');
                    setTimeout(function() {
                       $('#password').focus();
                    });
                }
            }
        }

    });
    /* ...and come back */
    $('.inputdefault').live("blur",function() {
        if ($(this).val() == '') {
            $(this).val($(this)[0].title);
            if($(this).hasClass('password')) {
                $(this).prop('type', 'text');
            }
        }
    });
	
	/* code needed for account pages only */
	
	$('a.close').click(function(evt) {
		//console.log($(this).parentsUntil('.head'))
		$(this).parentsUntil('.shipping').siblings().toggle();
		evt.preventDefault();
	});
	$('a.closeAddress').click(function(evt) {
		$(this).parentsUntil('.shipping').remove();
		evt.preventDefault();
	});
	
	$('div.form input[readonly="readonly"]').focus(function() {
		$(this).blur();
	});
	
	$('div.star.empty div.rating').hover(function() {
		$(this).prevUntil('div.feedback').toggleClass('active');
	});
	
	/* end: code needed for account pages only */

    /* code needed for seller profile pages only */
    $('#main .seller_review a').click(function(e) {
        e.preventDefault();
        $('.seller_review div').removeClass('selected');
		$(this).parent().addClass('selected');
	});
	/* end: code needed for seller profile pages only */

})( jQuery );
