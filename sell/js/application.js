(function($){
	

$.fn.errorInput = function(){
	
	return this.each(function(){
		
		$(this).addClass("error-input");
		$(this).bind("change.error keyup.error",function(){
			
			$(this).removeClass("error-input");
			
			$(this).unbind("change.error keyup.error");
		});
	});
	
}

var Application = {
	Model : {},
	View : {},
	Router : {},
	Language : {},
	Constants : {
		"iframe_upload_redirect" : window.location.href.split('#')[0] + "youtube.html",
		"youtube_service_url" : "http://video.epek.com/youtubeupload?jsoncallback=?",
		"youtube_video_info_url" : "https://gdata.youtube.com/feeds/api/videos/{id}?alt=json-in-script&callback=?",
		"image_upload_url" : 'http://upload-img.epek.com/uploadimage',
		"auction_post_url" : "http://2.sell.api.epek.com/listItem",
		"fixedprice_post_url" : "http://2.sell.api.epek.com/listFixedPriceItem"
	}
};
/*
 * Language 
 * 
 * */
 Application.Language.en = {
	 // Step 1 messages
	"step1_video_uploaded" : "Video uploaded, You may continue to the next steps",
	"step1_video_uploading_failed" : "Uploading video file failed..",
	"step1_add_video" : "Upload video file...",
	"step1_video_deleted" : "Video deleted...",
	"step1_loading_data" : "Loading step data...",
	"step1_video_processed" : "Video uploaded and processed successfully..",
	"step1_video_uploading" : "The video file is uploading ...You may continue to the next steps",
	// Step 2 messages
	"step2_add_images"	: "Upload some images..",
	"step2_image_uploaded" : "Image successfully uploaded",
	"step2_image_deleted" : "Image successfully deleted",
	"step2_image_uploading" : "Image is being uploaded",
	"step2_image_upload_failed" : "Image uploading failed",
	// Step 3
	"step3_title_validation_error" : "Please enter title",
	"step3_zip_validation_error" : "Please enter zip",
	"step3_zip_not_found" : "Zip code wasn't found",
	"step3_zip_searching" : "<img src='images/ajax-loader.gif' alt='Loading...'/> Checking Zip Code",
	"step3_zip_found" : "Zip code successfully validated",
	"step3_zip_validation_checking" : "Zip is being checked for validation",
	"step3_loca_error" : "Please select a valid location from the dropdown",
	"step3_desc_validation_error" : "Please enter description",
	"step3_validation_success" : "Successfully completed step",
	// Step 4
	"step4_select_category" : "Please select category",
	"step4_category_selected" : "Category successfully selected",
	"step4_category_validation_error" : "Please select leaf category",
	// Step 5
	"step5_enter_attributes" : "Please enter attributes",
	"step5_attributes_loading" : "Attributes for selected category are loading...",
	"step5_loaded_attributes_validation_error" : "Please enter all predefined attributes",
	"step5_custom_attributes_validation_error" : "Please enter all custom attributes",
	"step5_validated" : "Successfully entered all attributes",
	// Step 6
	"step6_select_auction_type" : "Please select auction type",
	"step6_auction_type_validation_error" : "Please select auction type",
	"step6_quantity_validation_error" : "Please enter quanitity",
	"step6_price_validation_error" : "Please enter price",
	"step6_starting_price_validation_error" : "Please enter starting price",
	"step6_enter_price_and_quantity" : "Please enter price and quantity",
	"step6_enter_starting_price" : "Please enter starting price",
	"step6_validated" : "Successfully validated",
	// Step 7
	"step7_enter_shipping_info" : "Please enter shipping info",
	"step7_shipping_validation_error" : "Please fill in all shipping info",
	"step7_validated" : "Successfully entered all shipping info",
	// Step 8
	"step8_enter_payment_info" : "Please enter payment info",
	"step8_payment_validation_error" : "Please select at least one payment method",
	"step8_paypal_email_validation_error" : "Please enter valid paypal email",
	"step8_google_merchant_id_validation_error" : "Please enter valid google merchant id" ,
	"step8_cheque_validation_error" : "Please enter cheque info",
	"step8_validated" : "Successfully entered payment info",
	"step8_video_uploading" : "Please wait for video to finish uploading...",
	"step8_images_uploading" : "Please wait for images to finish uploading...",
	"step8_zip_invalid" : "Location is invalid..."
	
 }
 
/* helper function for fetching words */
 function __( word , lang ){
	 if(lang == undefined ) lang = "en";
	 
	 return Application.Language[lang][word];
 }
/*
 * Main Model
 * 
 */
Application.Model.Main = Backbone.Model.extend({

  defaults : {
	   currentStep : 0
  },
  initialize: function() {
	  
	  this.steps = new Array();
	  
	  this.listing = false;
	  
	  this.listed = false;
	  
	  // create Navigation view
	  this.view = new Application.View.Main( { el : $("#main"), model : this  } );
	  
	  this.loader = new Application.View.Loader( { model : this , parent : $("#main") } ) ;
	  
	  this.authentication = new Application.View.Authentication( { model : this , parent : $("#main") } ) ;
	  
	  // Initialize all steps
	  this.steps.push( new Application.View.Step1( { model : this , el : $("#step1_content") } ) );
	  this.steps.push( new Application.View.Step2( { model : this , el : $("#step2_content") } ) );
	  this.steps.push( new Application.View.Step3( { model : this , el : $("#step3_content") } ) );
	  this.steps.push( new Application.View.Step4( { model : this , el : $("#step4_content") } ) );
	  this.steps.push( new Application.View.Step5( { model : this , el : $("#step5_content") } ) );
	  this.steps.push( new Application.View.Step6( { model : this , el : $("#step6_content") } ) );
	  this.steps.push( new Application.View.Step7( { model : this , el : $("#step7_content") } ) );
	  this.steps.push( new Application.View.Step8( { model : this , el : $("#step8_content") } ) );
	  
	  this.finalStep =  new Application.View.FinalStep( { model : this , el : $("#final_content") } );
	  
	  // initialize Hash rooter for navigation
	  this.router = new Application.Router.Nav({ model : this });
	  
	  // start tracking hash changes
	  Backbone.history.start();
  },
  totalSteps : function(){
	  return this.steps.length;
  },
  // Validate all steps that are lower than INDEX step
  validateSteps : function( index ){
	  
	  // if we are selecting first step, don't validate
	  if( index === 0 ){
		  return {passed : true}
	  }
	  
	  // if some step isn't valid this will change to false
	  var validated = true;
	  // if some step is invalid, its index will be stored here
	  var invalidStepIndex = null;
	  
	  $.each(this.steps.slice(0,index),function(i,step){

			if( validated && !step.validate()){
				validated = false;
				invalidStepIndex = i;
			}
	  });
	  
	  return {passed : validated, index : invalidStepIndex};
	  
  },
  goToFinalStep : function(){
	  
	  if(this.listed){
		  
		$("#menu li").not(".listed").removeClass("selected").hide();
		
		$("#menu li.listed").addClass("selected").show();
		
		$.each( this.steps , function( i, step ){
			step.deactivate();
		});
		
		this.finalStep.activate();
	}
	else {
		this.finalStep.deactivate();
		this.router.navigate("!step1",{ trigger : true });
	}
  },
  goToStep : function( index ){
	  
	  
	  // if selected step isn't already displayed , validate if we are allowed to proceed to selected step 
	  if( index < this.totalSteps() && this.get("currentStep") !== index ){
		  
		var validation = this.validateSteps( index );
		
		if( validation.passed ){
			// if all previous steps are valid, display selected step
			this.displayStep( index );
		}
		else {
			this.router.navigate("!step"+(validation.index+1),{ trigger : true });
		}
	  }
	  else if(this.get("currentStep") !== index){
		this.router.navigate("!step"+this.get("currentStep"),{replace: true, trigger : true });
	  }
  },
  displayStep : function( index ){
	$.each(this.steps,function( stepIndex, step ){
		if( stepIndex !== index ){
			// hide all steps other than currently selected
			step.deactivate();
		}
	});
	
	// show content for selected step  
	this.steps[index].activate();
	// mark selected step in navigation
	this.view.select( index );
	
	// save current step index
	this.set({ "currentStep" : index });
	
  },
  nextStep : function(){
	// proceed to next step if it exists
	if( this.get("currentStep") < this.totalSteps()) {
            var currentStep = this.get("currentStep");
            // Delete me when adding back categories
            var count = 1;
		this.router.navigate("!step"+(this.get("currentStep") + 1+count),{ trigger : true });
	}
  },
  generateFinalStep : function( viewItemUrl ){
	
	var data = {
		url : viewItemUrl,
		title : this.steps[2].getTitle(),
		description : "",
		duration : this.steps[5].getDuration(),
		price : this.steps[5].selected_type == "auction" ? this.steps[5].getStartingPrice() : this.steps[5].getPrice(),
		shipping : this.steps[6].getShippingCost( this.steps[6].shipping_wrapper.children().first() ),
		image : this.steps[1].getFirstImage()
	}
	

	this.finalStep.generate( data );
	
	this.router.navigate("!final",{ trigger : true });
	
	 $.each( this.steps , function( i, step ){
		
		step.reset();
		  
	 });
	  
  },
  
  waitAuthentication : function(){
	  
	  var self = this;
	  
	  // remove cookie if existant
	  $.cookie("sess_id","",{"expires":-1});
	  
	  self.loader.hide();
	  
	  this.authentication.show();
	  
	  var authenticationInterval = setInterval( function(){
		
			if( $.cookie("sess_id")!=null && $.cookie("sess_id").length > 0 ){
				
				clearInterval( authenticationInterval );
				
				//self.steps[7].notification.reset().setSuccess().add("You are logged in, proceed with listing item...");
				
				self.authentication.hide();
				
				self.listItem();
				
			}
		  
	  },100);
  },
  
  listAnotherItem : function(){
	  
	  this.listed = false;
	  
	  this.router.navigate("!step1",{ trigger : true });
	  
	  this.finalStep.reset();
	  
  },
  
  listItem : function(){
	  
	  var self = this;
	  
	  if(this.listing) return;
	  
	 var validation = this.validateSteps();
		
	 if(validation.passed){
		 
		 // this.listing = true;
		 
		 if( this.steps[0].isUploading() ){
			this.steps[7].notification.reset().setError().add("Please wait for video to finish uploading...");
			return false;
		 }
		 
		 if( this.steps[1].isUploading() ){
			this.steps[7].notification.reset().setError().add("Please wait for images to finish uploading...");
			return false;
		 }
		  
		  var postData = {};
		  var postURL = '';
		  
		  $.each( self.steps, function(i,step){
			
			postData = $.extend( {} , postData , step.getPostData() );
			  
		  });
		  
		  var postDataStr = JSON.stringify(postData);
 
		  if( self.steps[5].selected_type=="auction") {
			  postURL = Application.Constants.auction_post_url;
		  }
		  else {
			  postURL = Application.Constants.fixedprice_post_url;
		  }
		  
		  postURL += "?sessionId="+$.cookie("sess_id");
		  
				
		  self.loader.show();
		
		  $.ajax({             
			type: "POST",
            url: postURL,
            data: postDataStr,
            dataType:"json",
            contentType : "application/javascript",
			cache : false,
			success: function(response){
				
				 self.loader.hide();
				 
				 if(response.callStatus && response.callStatus.status == 'failure'){
					 
					 self.goToStep(7);
					 self.steps[7].notification.reset().setError().add("Error : " + response.callStatus.error.message ).show();
					 
				 }
				 else {
					 
					 if( response.itemUrl != undefined ){
						 
						 self.listed = true;
						 self.generateFinalStep( response.itemUrl );
						 
					 }
					 else {
						 
						 self.steps[7].notification.reset().setError().add("Item ID not valid").show();
						 
					 }
				 }
				  
				 this.listing = false;
		    },
		    error : function(qXHR, textStatus, errorThrown){
				  
				 self.goToStep(7);
				 self.steps[7].notification.reset().setError().add("Error : Status "+textStatus+" "+errorThrown).show();
				 self.loader.hide();
			}
		  });
		  
	  }
  }

});

/* Loader View */
Application.View.Loader = Backbone.View.extend({
	
  tagName : "div",
  
  className : "loader",
  
  initialize: function( options ) {  
	  
	this.$el.html("<div class='loader-image' ></div><div class='loader-bg' ></div>").hide();
	this.$el.appendTo( options.parent ); 
	
	
  },
  show : function(){  
	  this.$el.siblings().css("visibility","hidden");
	  this.$el.css("visibility","visible").show();
  },
  hide : function(){
	  this.$el.hide();
	  this.$el.siblings().css("visibility","visible");
  }

});

/* Authentication View */
Application.View.Authentication = Backbone.View.extend({
	
  tagName : "div",
  
  className : "authentication",
  
  initialize: function( options ) {  
	  
	this.$el.html("You are not signed in. Click <a href='https://signin.epek.com/login?serviceurl=http%3a%2f%2fstack.epek.com%2fsell%2fclose.html' target='_blank'><strong>HERE</strong></a> to login or signup.").hide();
	this.$el.appendTo( options.parent ); 
	
	
  },
  show : function(){  
	  this.$el.siblings().css("visibility","hidden");
	  this.$el.css("visibility","visible").show();
  },
  hide : function(){
	  this.$el.hide();
	  this.$el.siblings().css("visibility","visible");
  }

});

/* Main View */
Application.View.Main = Backbone.View.extend({
	
  initialize: function() {  
	  
	this.links = this.$el.find("#menu li");
	
  },
  select : function( index ){  
	  // deselect all links except new one
	  this.links.not(index).removeClass("selected");
	  // select new link
	  this.links.eq(index).addClass("selected"); 
  }

});

/* Notification Constructor */
 Application.View.Notification = Backbone.View.extend({

  tagName : "div",
   
  className : "notification",
  
  initialize : function( options ){
	  this.$el.appendTo(options.model.$el);
	  this.$box = $("<ul></ul>").appendTo($("<div></div>").appendTo( this.$el ));
  },
  
  show : function() {
          this.$box.parent().hide();
          this.$box.parent().jmNotify({
 		  'closeText': 'x',
		  'delay': 4000,
                  'speedIn' : 0,
                  'speedOut' : 0
	  });
	  return this;
  },
  
  hide : function(){
          return this;
  },
  
  reset : function(){
	  this.$box.empty();
      return this;
  },
  
  add : function( text ){ 
	  this.$box.append("<li>"+text+"</li>");	  
	  return this;
  },
  
  setError : function(){
	  this.$box.parent().removeClass("success warning info").addClass("error")
	  return this;
  },
  
  setSuccess : function(){
	  this.$box.parent().removeClass("error warning info").addClass("success");
	  return this;
  },
  
  setWarning : function(){
	  this.$box.parent().removeClass("success error info").addClass("warning");
	  return this;
  },
  
  setValidationError : function(){
	  this.$box.parent().removeClass("success warning info").addClass("error");
	  return this;
  },
  
  setImportant : function(){
	  this.$box.parent().removeClass("success error warning").addClass("info");
	  return this;
  }

});

/* Autocomplete Constructor */
 Application.View.Autocomplete = Backbone.View.extend({
	
	tagName : "div",
	
	className : "autocomplete",
	
	initialize : function( options ){
		
		var self = this,
			$parent = options.appendTo;
		
		this.$el.appendTo($parent);
		
		this.$list = $("<ul></ul>").appendTo(this.$el);
		
		this.input = $parent.find("input");
		
		this.items = options.items;
		
		var timeout;
		
		this.input.bind("focus",function(){
			
			clearTimeout(timeout);
			
			var inputvalue = self.input.val();
			
			self.$list.empty();
			
			self.input.data("valueId", undefined);
			
			$.each(self.items,function(valueId,value){
				
				if( inputvalue=='' || ( inputvalue!='' && value.toLowerCase().indexOf(inputvalue.toLowerCase()) == 0) ){
					var $li = $("<li><a href='#' >"+value+"</a></li>");
					self.$list.append( $li );
					self.valueId = null;
					$li.find("a").bind("click",function(e){
						e.preventDefault();
						self.input.val(value);
						self.input.data("valueId", valueId);
						
					});
					
					if( inputvalue.toLowerCase() == value.toLowerCase() ){
						self.input.data("valueId", valueId);
					}
				}
				
			});
			
			self.$el.show();
			
		}).bind("blur",function(){
			timeout = setTimeout(function(){ self.$el.hide(); },100);
		}).bind("keyup",function(e){
			
			if (e.keyCode == 38) {
				var over = self.$list.find('li.over').removeClass('over');
				if (over.length && over.prev().length) {
					var prev = over.prev();
				} else {
					var prev = self.$list.find('li:last');
				}
				prev.addClass('over');
				self.input.val(prev.text());
			} else if(e.keyCode == 40) {
				var over = self.$list.find('li.over').removeClass('over');
				if (over.length && over.next().length) {
					var next = over.next();
				} else {
					var next = self.$list.find('li:first');
				}
				next.addClass('over');
				self.input.val(next.text());
			} else {
				var inputvalue = self.input.val();
				
				self.$list.empty();
				
				self.input.data("valueId", undefined );
						
				$.each(self.items,function(valueId,value){
					
					if( inputvalue=='' || ( inputvalue!='' && value.toLowerCase().indexOf(inputvalue.toLowerCase()) == 0) ){
						var $li = $("<li><a href='#' >"+value+"</a></li>");
						self.$list.append( $li );
						$li.find("a").bind("click",function(e){
							e.preventDefault();
							self.input.val(value);
							self.input.data("valueId", valueId);
						});
						
						if( inputvalue.toLowerCase() == value.toLowerCase() ){
							self.input.data("valueId", valueId);
						}
					}
					
				});
			}
			
			
			
		});
		
	}

});

/* ProgressBar Constructor */
 Application.View.ProgressBar = Backbone.View.extend({
  
  initialize : function(){
	  
  },
  reset : function(){
	  
	  this.go( 0 );
	  
  },
  go : function( percent , animate , remove ){
	  
	  var self = this;
	  
	  if(animate == undefined ) animate = false;
	  
	  if(animate){
		  this.$el.stop().animate({ width : this.$el.parent().width()*percent }, 300, "linear" , function(){ $(this).css("width",( percent * 100 ) + "%"); if ( remove ) self.$el.parent().fadeOut(); } );
	  }
	  else {
		  this.$el.stop().css("width",( percent * 100 ) + "%");
	  }
	  
  }

});

/* Step Constructor */
 Application.View.Step = Backbone.View.extend({
  
  skip : function( event ){
	  
	   this.model.nextStep();
	   return false;
  },
  next : function( event ){
	   this.model.nextStep();
	   return false;
  },
  validate : function(){
	  return true;
  },
  activate : function(){
	  this.$el.addClass("active");
  },
  
  deactivate : function(){
	  this.$el.removeClass("active");
  },
  
  reset : function(){
	  
  },
  
  getPostData : function(){
	  
	return  "";
		
  }

});

/* Step 1 - Upload Video */
Application.View.Step1 =  Application.View.Step.extend({
  
  events: {
    "click a.skip"   		: "skip",
    "click a.next"   		: "next",
    "click .delete"  		: "removeVideo",
    "change .button input" 	: "upload_video",
    "click .button input" 	: "isNotUploading"
  },
  initialize : function(){
	  
	  var self = this;
	  
	  // initialize notification box
	  this.notification = new  Application.View.Notification({ model : this });
	  
	  // ajax request
	  this.ajax = null;
	  
	  this.ajaxTimeout = null;
	  
	  // cache videos container
	  this.video_box = this.$el.find("ul.video_upload");
	  
	  this.video_box.show();
	  
	  // generate video HTML
	  this.video = $('<li><a href="#"><img src="images/2.jpg" width="240" alt="Sample Video"><div class="delete">Remove</div></a></li>').hide().appendTo( this.video_box );
	  
	  this.videoId = null;
	
	  // upload indicator
	  this.indicator = this.model.view.links.eq(0).find(".indicator");
	  
	  // flag so that we know if uploading is running
	  this.uploading = false;  
	  
	  // hide form at start so that it can't be clicked until our first JSON call returns Youtube ID
	  this.form = this.$el.find("a.button form").hide();
	  
	  this.createIframe();
	  
	  this.form.parent().css("opacity",0.5);
	  
	  this.getToken();
		
  },
  
  getToken : function(){
	
	 var self = this;
	 
	  // set default notification
	  this.notification.setImportant().add( __("step1_loading_data") ).show();
	  
	  $.ajax({
		type: "GET",
		url:  Application.Constants.youtube_service_url ,
		dataType: "jsonp",
		cache : false,
		success: function(data) {
			
			self.notification.reset().setImportant().add( __("step1_add_video") ).show();
			
			// set action attribute on form so that video is uploaded to youtube and that redirect url is page on our domain
			self.form.attr('action',  data.url + "?nexturl="+encodeURIComponent( Application.Constants.iframe_upload_redirect ) );

			// set youtube token
			self.form.find("#token").val(data.token);
			
			// form is ready, display it now
			self.form.show();
			
			// set full opacity on browse button
			self.form.parent().css("opacity",1);
		}
	  });
	  
  },
  
  createIframe : function(){
	  
	  var self = this;
	  
	  // iframe for upload
	  this.iframe = $(" <iframe name='video_upload_form' id='video_upload_form' src='blank.html' ></iframe>").bind("load",function(){
	
			// get iframe URL 
			var iframe_href= this.contentWindow.location.href;
			
			// we have to check if iframe href has blank.html in it, because load event will fire first time we place iframe on the page
			if( iframe_href && iframe_href.indexOf("blank.html")===-1){
				
				var urlParams = {};
				var e,
				a = /\+/g,  // Regex for replacing addition symbol with a space
				r = /([^&=]+)=?([^&]*)/g,
				d = function (s) { return decodeURIComponent(s.replace(a, " ")); },
				q = iframe_href.split("?")[1];
					
				// get all query paremeters info urlParams object
				while (e = r.exec(q))
					urlParams[d(e[1])] = d(e[2]);
					
			    // check if video was successfully uploaded
				if(urlParams.status &&  urlParams.status == "200") {
					
					self.video.find( "img" ).attr("src",'http://img.youtube.com/vi/'+urlParams["id"]+'/2.jpg');
					
					// start checking to see if video was processed
					self.video_uploaded( urlParams["id"] );
					
					// show notification that video was uploaded
					self.notification.reset().setImportant().add(  __("step1_video_uploaded") ).show();
				}
				else {
					
					// show error notification
					self.notification.reset().setError().add( __("step1_video_uploading_failed") ).show();
					
				}
				
				self.stop_indicator();
				
				// uploading process is complete
				self.uploading = false;
			}
	  }).appendTo(this.$el);
  },
  
  reset : function(){
		
		clearTimeout( this.ajaxTimeout );
		
		if( this.ajax ){
			this.ajax.abort();
		}
		
		this.videoId = null;
	  
  },
  
  video_uploaded : function( id  ){
	  
	var self = this,youtube_id = id;
	
	this.videoId = youtube_id;
	
	this.ajax = $.ajax({
		type: "GET",
		url:  Application.Constants.youtube_video_info_url.replace("{id}",id) ,
		dataType: "json",
		success: function(data) {
			
			// if processing has finished
			if (!data.entry.app$control || data.entry.app$control.yt$state.name != "processing" ) {
				
				// add timestamp onto image URL so that it gets refreshed
				self.video.find("img").replaceWith('<img src="http://img.youtube.com/vi/'+youtube_id+'/2.jpg?'+(new Date().getTime())+'" width="240" alt="Sample Video" />');
					
				// show success notification
				self.notification.reset().setSuccess().add( __("step1_video_processed") ).show();
				
				// browser button is enabled again
				self.form.parent().css("opacity",1);
				
				self.ajax = null;
			}
			else {
				// add timestamp onto image URL so that it gets refreshed
				self.video.find("img").replaceWith('<img src="http://img.youtube.com/vi/'+youtube_id+'/2.jpg?'+(new Date().getTime())+'" width="240" alt="Sample Video" />');
				// check processing status again in one minute
				self.ajaxTimeout = setTimeout(function() { self.video_uploaded( youtube_id ); } ,30*1000);
			}
		},
		error : function(){
			// check processing status again in one minute
			self.ajaxTimeout = setTimeout(function() { self.video_uploaded( youtube_id ); } ,30*1000);
		}
	
	});
			
  },
  upload_video : function( event  ){
	  
	  // cancel if we are already uploading
	  if( this.uploading ) return;
	  
	  // set uploading flag
	  this.uploading = true;
	  
	  // fade out button so that it marks it as disabled
	  this.form.parent().css("opacity",0.5);
	  
	  // start upload
	  this.form.submit();
	  
	  this.video.show();
	  
	  // reset video id
	  this.videoId = null;
	  
	  // abort Ajax call if there is any
	  if( this.ajax ){
		  this.ajax.abort();
	  }
	  
	  // clear Ajax timeout
	  clearTimeout(this.ajaxTimeout);
	  
	  // hide SKIP button
	  this.$el.find(".skip").hide();
	  
	  // show NEXT button
	  this.$el.find(".next").fadeIn();
	  
	  // show notification
	  this.notification.reset().setImportant().add( __("step1_video_uploading") ).show();
	  
	  this.start_indicator();
	 
  },
  
  start_indicator : function(){
	  
	  var self = this;
	  
	  this.stop_indicator();
	  
	  this.indicator.animate({ "width" : this.indicator.parent().width() },750,function(){
		
		  self.start_indicator();
	  });
	  
  },
  
  stop_indicator : function(){
	  
	  this.indicator.stop().width(0);
  },
  
  removeVideo : function( event ){
	
	var self = this;
	// remove video
	$( event.currentTarget ).parents("li").fadeOut(function(){
	
		$(this).remove();
	});
	
	// reset video id
	this.videoId = null;
	
	// abort Ajax call if there is any
	 if( this.ajax ){
		  this.ajax.abort();
	 }
	  
	 // clear Ajax timeout
	 clearTimeout(this.ajaxTimeout);
	 
	// fade in button so that it marks it as enabled
	this.form.parent().css("opacity",1);
	
	 // hide NEXT button
	 this.$el.find(".next").hide();
	 
	 // show SKIP button
	 this.$el.find(".skip").fadeIn();
			
	self.notification.reset().setImportant().add( __("step1_add_video") ).show();
	 
	return false;  
  },
  
  isUploading : function(){
	   
	   // if video is being uploaded cancel click event on button
	   return this.uploading;
  },
  
  isNotUploading : function(){
	   
	   // if video is being uploaded cancel click event on button
	   return !this.uploading;
  },
  
  getVideo : function(){
	  
	  return this.videoId;  
  },
  
  getPostData : function(){
	  
		var postData = {};
		
		if(this.videoId){
			postData.video = [this.getVideo()];
		}
		
		return postData;
	
  }

});

if( $.browser.msie && Number($.browser.version) < 10 ){
	
	$.fn.uploader = function( options ){
		
		var defaults = {
			url : "",
			onComplete : null,
			onStart : null,
			onProgress : null
		};
		
		var opts = $.extend( defaults, options );
		
		return this.each(function(){
			
			var uploader = $(this);
			
			uploader.uploadify({
				'uploader'  : 'js/uploadify.swf',
				'script'    : Application.Constants.image_upload_url,
				'cancelImg' : 'js/cancel.png',
				'auto'      : true,
				'scriptAccess' : 'always',
				'width' 		: 212 ,
				'height'		: 58,
				'fileExt'     : '*.jpg;*.gif;*.png',
				'fileDesc'     : ' ',  
				'wmode'       : 'transparent',
				'hideButton'  : true,
				'multi' : true,
				onAllComplete : function(){
					//console.log("onAllComplete");
					//console.log(arguments);
				},
				onComplete : function( e , id , file, response ){
					//console.log("onComplete");
					//console.log(arguments);
					
					//console.log(JSON.parse( response ) )
					opts.onComplete( id , JSON.parse( response ) );
				},
				onSelect : function( e, id, file){
					//console.log("onSelect");
					//console.log(arguments);
	
					opts.onStart( id , file.size , file);
				},
				onError : function(){
					//console.log("onError");
					//console.log(arguments);
				},
				onProgress : function( e, id , file, progress){
					//console.log("onProgress");
					//console.log(arguments);
					opts.onProgress( id ,Math.round( file.size * progress.percentage / 100 ), file.size , { abort : function(){ uploader.uploadifyCancel( id ) } } );
				}
			});
			
			
		});
		
	};
	

}
else {

	$.fn.uploader = function( options ){
		
		var defaults = {
			url : "",
			onComplete : null,
			onStart : null,
			onProgress : null
		};
		
		var opts = $.extend( defaults, options );
		
		return this.each(function(){
			
			var uploader = $(this);
			
			uploader.fileupload({
				
				dataType: 'json',
				
				url: Application.Constants.image_upload_url,
				
				done: function (e, data) {
					
					opts.onComplete( data.unique_id , data.result );
					
				},

				progress : function( e, data ){
					
					opts.onProgress( data.unique_id , data.loaded , data.total , data.xhr() );
					
					return true;
				},
				add : function(e, data){
					
					var id = String ( new Date().getTime() ) ;
					
					var allowedTypes = [ "image/png" , "image/jpeg" , "image/gif" ];
					
					data.unique_id = id;
					
					if( $.inArray( data.files[0].type , allowedTypes) !== -1 ){
					
						opts.onStart( id , data.files[0].size , data.files[0]);
						
						data.submit();
					}
					
				},
				start : function(e,data){
					
					return true;
				}
			});
		});
		
	};
		
}

/* Step 2 - Upload Images */
Application.View.Step2 =  Application.View.Step.extend({
  
  events: {
    "click a.skip"   : "skip",
    "click a.next"   : "next"
  },
  
  initialize : function(){
	  
	var self = this;
	  
	 // initialize notification box
	this.notification = new  Application.View.Notification({ model : this });
	    
	// list of all images
	this.images = {};
	 
	// image container
	this.imageList = this.$el.find("ul.image_upload");
	
	// upload indicator
	this.indicator = new Application.View.ProgressBar({ el : this.model.view.links.eq(1).find(".indicator") });
	
	this.images_being_uploaded = 0;
	
	this.uploading_images = [];
	
	// button for uploading images
	this.imgupload = this.$el.find(".image_upload .button input");
	
	this.imgupload.uploader({
	
		onStart : function( id, size , file  ){
			
			self.notification.reset().setImportant().add( __("step2_image_uploading") ).show();
			
			self.add( id , size , file);
			
		},
		
		onComplete : function( id, response ){
			
			self.complete ( id , response );
					
			if( self.uploading_images.length == 0 ){
				
				self.indicator.go(1,true);
			}
			else {
				
				var total = 0;
				
				var uploaded = 0;
				
				$.each( self.uploading_images , function( i , uploading_image ){
			
					total += uploading_image.size ;
					uploaded += uploading_image.uploaded ;
					
				});
				
				var progress = parseFloat(uploaded / total, 10);
			
				// show current progress on indicator
				self.indicator.go(progress,true);
				
			}
			
		},
		onProgress : function( id , uploaded, size , api ){
			
			var progress = parseFloat( uploaded / size , 10);
			
			if( self.images[ id ]== undefined ){
				
				api.abort();
					
			}
			else {
				
				$.each( self.uploading_images , function( i , uploading_image ){
			
					if( uploading_image.id == id ){
						
						self.uploading_images[i].uploaded = uploaded;
						
						self.uploading_images[i].progressbar.go(progress,true);
						
					}
					
				});
				
				
			}
			
			var total = 0;
			
			var uploaded = 0;
			
			$.each( self.uploading_images , function( i , uploading_image ){
		
				total += uploading_image.size ;
				uploaded += uploading_image.uploaded ;
				
			});
			
			if(total > 0){
				var progress = parseFloat( uploaded / total, 10);
				// show current progress on indicator
				self.indicator.go(progress,true);
			}
		}
		
	});
    
    self.notification.reset().setImportant().add( __("step2_add_images") ).show();
    
  },
  
  reset : function(){
		
		
	  
  },
  
  isUploading : function(){
	  
	  return this.uploading_images.length > 0;
	  
  },
  
  add : function( id, size , file ){
	
	var self = this;
	
	this.images[ id ]= {};
	
	this.images[ id ].imageData = null;
			
	var $imageLI = $('<li id="image_'+id+'"><a href="#"><span class="delete">Remove</span></a><div class="progressbar" ><div class="indicator" ></div></div></li>');
	
	this.uploading_images.push({ id : id , size : size , uploaded : 0 , progressbar : new Application.View.ProgressBar( { el : $imageLI.find(".indicator") } ) });
	
	// add delete event and insert it into image container
	$imageLI.find("a").bind("click",function(e){
		
		e.preventDefault();
		
		// delete image
		self.delete_image( id );
		
	}).parent().appendTo( this.imageList );
	
	this.images[ id ].imageWrapper = $imageLI.find("a");
	
	if (typeof FileReader !== 'undefined' && FileReader.prototype.readAsDataURL && ( !$.browser.msie || ( $.browser.msie && Number($.browser.version) > 10) ) ) {
          var fileReader = new FileReader();
           fileReader.onload = function (e) {
			   if( self.images[ id ].imageData == null ){
				   $("<img width='120'/>").attr("src",e.target.result).hide().prependTo( $imageLI.find("a") ).fadeIn();
			   }
           };
           
           fileReader.readAsDataURL(file);
    }
	
  },
  
  complete : function( id , response ){

	var self = this;
	
	if( this.images[ id ] == undefined ) return;
	
	var newUploadingImages = [];
	
	$.each( self.uploading_images , function( i , uploading_image ){

		if( uploading_image.id == id ){
			
			self.uploading_images[i].progressbar.go(1,true,true);
			
		}
		else {
			
			newUploadingImages.push( uploading_image );
		}
	});
	
	self.uploading_images = newUploadingImages;
	
	this.images[ id ].imageData = response[0];
	
	this.images[ id ].imageWrapper.find("img").remove();
	
	if( this.images[ id ].imageWrapper.find("img").size() > 0){
		
		this.images[ id ].imageWrapper.find("img").attr('src',this.images[ id ].imageData.url + '=s120');
	}
	else {
		
		this.images[ id ].imageWrapper.prepend( $("<img/>").attr('src',this.images[ id ].imageData.url + '=s120') );
		
	}
	
	// hide SKIP button
	this.$el.find(".skip").hide();
	  
	  // show NEXT button
	this.$el.find(".next").fadeIn();
	
	// show success notification
	self.notification.reset().setSuccess().add( __("step2_image_uploaded") ).show();
	
	
  },
  
  delete_image : function( unique_id ){
	  
	var self = this;
	 
	// fade out image and remove it
	$("#image_"+unique_id).fadeOut(300,function(){$(this).remove();})
		
	var has_images = false;
	
	// check if there is any image in array
	for(var i in this.images ){
		has_images = true;
	}
	 if(!has_images){
		 
		// hide NEXT button
		this.$el.find(".next").hide();
		
		// show SKIP button
		this.$el.find(".skip").fadeIn();
	 }
	 
	 if(this.images[unique_id].imageData!==null){
		 // trigger ajax call for removing image
		 $.ajax({
			type : this.images[unique_id].imageData.delete_type,
			url : this.images[unique_id].imageData.delete_url 
		 });
	 }
	 
	// remove image data from image list
	delete this.images[String(unique_id)];
	
	var newUploadingImages = [];
	
	$.each( self.uploading_images , function( i , uploading_image ){
	
		if( uploading_image.id !== unique_id ){
			
			newUploadingImages.push( uploading_image );
			
		}
		
	});
	
	self.uploading_images = newUploadingImages;
	
			
	if( self.uploading_images.length == 0 ){
		
		self.indicator.reset();
	}
	else {
		
		var total = 0;
		var uploaded = 0;
		
		$.each( self.uploading_images , function( i , uploading_image ){
	
			total += uploading_image.size ;
			uploaded += uploading_image.uploaded ;
			
		});
		var progress = parseFloat(uploaded / total, 10);
	
		// show current progress on indicator
		self.indicator.go(progress,true);
		
	}
	
	// show success notification
	this.notification.reset().setSuccess().add( __("step2_image_deleted") ).show();
  },
  
  getFirstThumb : function(){
	
	var firstThumb = '';
	
	for( var i in this.images ){
		
		if(firstThumb == '' ){
			firstThumb = this.images[i].imageData.url + '=s120';
		}
		
	}
	
	return firstThumb;
	  
  },
  
  getFirstImage : function(){
	
	var firstImage = '';
	
	for( var i in this.images ){
		
		if(firstImage == '' ){
			firstImage = this.images[i].imageData.url;
		}
		
	}
	
	return firstImage;
	  
  },
  
  getPostData : function(){
	  
		var postData = {} ;
		
		postData.image = [];
		
		for( var i in this.images ){
			postData.image.push( this.images[i].imageData.url );
		}
		
		return postData;
		
  }

});

/* Step 3 - Description  */
Application.View.Step3 =  Application.View.Step.extend({
  
  events: {
    "click a.skip"   			: "skip",
    "click a.next"   			: "next"
  },
  
  initialize : function(){
	 
	var self = this;
	
	this.notification = new  Application.View.Notification({ model : this });
    
	CKEDITOR.replace('form_desc_desc', {
	
        toolbar :
            [
                { name: 'font', items: [ 'Font','FontSize' ] },
                { name: 'separator', items: [ '-' ] },
                { name: 'position', items: [ 'JustifyLeft','JustifyCenter','JustifyRight' ] },
                { name: 'separator', items: [ '-' ] },
                { name: 'decoration', items : [ 'Bold','Italic','Underline','Strike' , 'TextColor' ] },
                { name: 'separator', items: [ '-' ] },
                { name: 'smiley', items: [ 'Smiley' ,  'Link' ] }
            ],
        skin : 'chris',
        height : '350',
        removePlugins : 'elementspath'
    });

	
	this.inputs = {};
	
	// description input
	this.inputs.title = this.$el.find("#form_desc_titl");
	this.inputs.title.data("default",this.inputs.title.attr("title"));
	// location input
	this.inputs.loca = this.$el.find("#form_desc_loca"),
	// description input
	this.inputs.description = CKEDITOR.instances.form_desc_desc;
	this.inputs.description_default = this.$el.find("#form_desc_desc").attr("title")
	this.inputs.description.setData(this.inputs.description_default);
	// condition input
	this.inputs.condition = this.$el.find("#form_desc_cond");
	
	/* google autocomplete for address */
	var ac_options = {
		types: ['geocode'],
		componentRestrictions: {country: 'us'}
	}
	street1_ac = new google.maps.places.Autocomplete(this.inputs.loca.get(0), ac_options);
	google.maps.event.addListener(street1_ac, 'place_changed', function() {
		var place = street1_ac.getPlace();
		self.inputs.loca.data('place', place);
		self.inputs.loca.data('typed_value', self.inputs.loca.val());
	});
	
  },
  getPostData : function(){
	  
		var postData = {};
		
		postData.title = this.getTitle();
		postData.description = this.getDescription();
		postData.location = this.getLoca();
		
		return postData;
		
	
  },
  
  getTitle : function(){
	
	return  this.inputs.title.val();
  },
  
  getDescription : function(){
	  
	  return this.inputs.description.getData();
  },
  
  getLoca : function(){
	  var loc = this.inputs.loca.data('place').geometry.location;
	  return loc.lat() + ',' + loc.lng();
  },
  
  validate : function(){
	  
	this.notification.reset();
	
	var validated = true;
  
	// check if title is longer than 2 characters
	if( this.inputs.title.val() == this.inputs.title.data("default") || trim( this.inputs.title.val() ) < 3 ){
		// add error message
		this.notification.add(__("step3_title_validation_error"));
		// validation failed
		validated = false;
		// add error class
		this.inputs.title.errorInput();
	}

	/*
	if( this.inputs.description.getData().indexOf(this.inputs.description_default)!==-1 || trim( this.inputs.description.getData() ) < 3 ){
		// add error message
		this.notification.add( __("step3_desc_validation_error") );
		$("span#cke_form_desc_desc").css("border","1px solid red");
		// validation failed
		validated = false;
	}
	else {
		$("span#cke_form_desc_desc").css("border","0px solid red");
	}
	*/

	// check if location has a valid selected place, and it hasn't changed since selection
	if (!this.inputs.loca.data('place') || this.inputs.loca.val() != this.inputs.loca.data('typed_value')) {
		this.notification.add( __("step3_loca_error") );
		validated = false;
		this.inputs.loca.errorInput();
	}
	
	if(!validated){
		// show validation errors
		this.notification.setValidationError().show();
		
	}
	else {
		// show success notification
		this.notification.setSuccess().add( __("step3_validation_success") ).show();
		
	}
	
	return validated;
  }

});

/* Step 4 - Categories */
Application.View.Step4 =  Application.View.Step.extend({
  
  events: {
    "click a.skip"   : "skip",
    "click a.next"   : "next"
  },
  
  initialize : function(){
	  
	 // initialize notification box
	this.notification = new  Application.View.Notification({ model : this });
	
	this.notification.setImportant().add( __("step4_select_category") ).show();
	
	// are categories loading
	this.loading = false;
	
	// ajax request object
	this.ajax = null;
	
	// currently selected category, breadcrumbs are generated from it
	this.selected_categories = new Array();
	
	// load initial categories
	this.load_categories();
	
	// category wrapper, and set scrollLeft to default
	this.category_list = this.$el.find("div.step_category_wrapper")
	
	this.category_list.parent().scrollLeft(0);
	
	// breadcrumb container
	this.breadcrumbs_list = this.$el.find("div.category_breadcrumbs");
	
	// selected category id
	this.category_id = null;
	
  },
  
  load_categories : function(path){
	 var depth = path ? path.split('/').length : 0;
	 var self = this;
	 
	 if(this.cataxx && this.cataxx.abort){
		 this.cataxx.abort();
	 }
	 
	this.cataxx = $.getJSON('http://data.epek.com/US_EN/' + (path ? path + '/' : '') + 'category', function (r) {
		self.render_category(r, depth);
	}).complete(function () {
		self.cataxx = null;
	})
  } 
  , bound_categories : function( category_level ){
	  
	  var self = this;
	  
	  // bound all selected categories
	  $.each(self.selected_categories,function( i, li ){
		 
		  $(li).addClass("selected").find(".connect").addClass("on").css({
			  "top" : 0,
			  "height" : 58
		  });
		  
		  if(i!==self.selected_categories.length-1){
			  
			  var currentPos = $(li).offset().top;
			  
			  var nextPos = self.selected_categories[i+1].offset().top;
			  
			  var top = nextPos - currentPos;
			  
			  var height = Math.abs(top)+58;
			  
				if( top > 0 ){
				
				  $(li).find(".connect").css({
					  "top" : 0,
					  "height" : height
				  });
				  
				}
				else {
					
				  $(li).find(".connect").css({
					  "top" : top,
					  "height" : height
				  });
				  
				}
			  
		  }
	  });
	  
  }
  , render_category : function (cat, depth){
	 
	  var self = this ,
		  $category_list;
	 
	  if (depth === 0){
		  
		  // on start, clear category list
		  this.category_list.find("ul").remove();
		  // reset position to 0
		  this.category_list.parent().scrollLeft(0);
		  
		  this.category_list.css("width","100%");
		  // create category tree element
		  var $category_list = $("<ul class='col_one' ></ul>");
		  // add category tree to container
		  $category_list.prependTo(this.category_list);
		   
		  this.notification.reset().setImportant().add( __("step4_select_category") ).show();
		  
		  this.category_list.css("height","auto");
	  }
	  else {
		  // set width of categories wrapper so that next levels can fit, I added 10 multiplier to avoid breaking into new rows
		  this.category_list.width((depth+10)*this.category_list.parent().width()/2);
		  
		  // if we got less than three levels, set position to 0 since both category trees will fit on page
		  if(depth < 2){
			  this.category_list.parent().scrollLeft(0);
			  self.category_list.css("height","auto");
		  }
		  // else scroll so that we can see last two category trees on page
		  else {
			this.category_list.parent().animate({"scrollLeft":Math.max((depth-1)*this.category_list.parent().width()/2,0)},300,function(){
					
				if(depth>1){
					self.category_list.css("height",self.category_list.children().last().prev().outerHeight(true));
				}
				else {
					self.category_list.css("height","auto");
				}
			});
	      }
		
		  // remove category trees that are equal or higher than new level
		  this.category_list.find("ul").filter(":gt("+(depth-1)+")").remove();
		  
		  // create next category tree
		  var $category_list = $("<ul class='col_two' ></ul>");
		
		  // insert it after last category tree
		  $category_list.insertAfter( this.category_list.find("ul").last() );
	  }
	  
	  // remove highlight classes from previously selected elements
	  $.each(self.selected_categories,function( i, li ){
		  $(li).removeClass("selected").find(".connect").removeClass("on");
	  });
	  
	  self.selected_categories = self.selected_categories.slice(0, depth);
	  
	  this.bound_categories(depth);
	  
	  $.each(cat.meta.children, function(key, subcat){
		  var subdepth = subcat.CategoryPath.split('/').length;
		  
		  // create category item
		  var $li = $("<li><a href='#' >"+subcat.CategoryName+(subcat.LeafCategory?"<span class='arrow'></span>":"<span class='connect' ></span>")+"</a></li>");
			  
		  $li.find("a").bind("click",function(e){
			  
			  e.preventDefault();
			  // remove categories that are higher or equal to this category level
			
			  
			  // remove highlight classes from previously selected elements
			  $.each(self.selected_categories,function( i, li ){
				  $(li).removeClass("selected").find(".connect").removeClass("on");
			  });
			  
			  self.selected_categories = self.selected_categories.slice(0,subdepth);
			  
			  // save our new category element
			  self.selected_categories[subdepth-1] = $li;
	
			  self.bound_categories(subdepth);
	
			  // if it is leaf category select it, and regenerate breadcrumbs
			  if(subcat.LeafCategory){
				    // select category
					self.select_category( subcat);
					// regenerate breadcrumbs
					self.generate_breadcrumbs(subcat);
				    // remove category trees that are equal or higher than new level
				    self.category_list.find("ul").filter(":gt("+(subdepth-1)+")").remove();
				    
					self.category_list.parent().animate({"scrollLeft":Math.max((subdepth-2)*self.category_list.parent().width()/2,0)},300,function(){
							
						if(subdepth>1){
							self.category_list.css("height",self.category_list.children().last().prev().outerHeight(true));
						}
					});
			  }
			  else {
		  
					// regerate breadcrumb to show loading gif
					self.generate_breadcrumbs(subcat, true);
				  
					$("html,body").animate({ "scrollTop" : $category_list.offset().top - 120 } , 300);
					
				    // deselect currently selected category
					self.select_category( null );
					// load category tree
					self.load_categories( subcat.CategoryPath );
			  }
		  });
		  
		  // add category item
		  $category_list.append($li);
		  
	  });
	  // regerate breadcrumb after loading categories
	  this.generate_breadcrumbs( cat.category );
	  
	  
	  this.select_category(cat.LeafCategory ? cat.category : null);
  },
  
  select_category : function(category){
	  
	  var self = this;
	  
	  // set currently selected category id
	  this.category_id = category ? category.CategoryPath : null;
	
	  if(this.category_id !== null){
		// category is selected, show success message
		this.notification.reset().setSuccess().add( __("step4_category_selected") ).show();
		
		// show NEXT button
		this.$el.find("div.next").fadeIn();
		
	  }
	  else {
		// category is not selected , show notification for selection of category
		this.notification.reset().setImportant().add( __("step4_select_category") ).show();
		
		// hide NEXT button
		this.$el.find("div.next").hide();
	  } 
	  
	  // trigger category changed event so that we can load attributes for this category on next step
	  this.model.trigger("category_changed", category);
  },
  
  generate_breadcrumbs : function( category , loading  ){
	  var depth = category.CategoryPath.split('/').length;
	  
	  var self = this;
	  // create breadcrumb and set it to load categories on click
	  var $a = $("<a href='#' >"+(category.CategoryPath == ''?"Home":category.CategoryName)+"</a>").bind("click",function(e){
		  e.preventDefault();
		  
		  // regerate breadcrumb to show loading gif
		  self.generate_breadcrumbs( category, true );
		  self.load_categories( category.CategoryPath );
	  });
	  
	  this.breadcrumbs_list.find(".breadcrumb_loading").remove();
	  
	  // if it is not home category
	  if(depth != 0){
		  // remove all breadcrumb items that are greater than currently selected category level
	      this.breadcrumbs_list.find("a").filter(function(i){
			 return i >= depth;  
		  }).remove();
		  // remove all separators too that are greater than currently selected category level
		  this.breadcrumbs_list.find("span").filter(function(i){
			 return i >= depth -1;  
		  }).remove();
		  
		  // add separator
		  this.breadcrumbs_list.append("<span>&gt;&gt;</span>");
	  }
	  else {
		  // remove all breadcrumb items
	      this.breadcrumbs_list.find("a").remove();
		  // remove all separators
		  this.breadcrumbs_list.find("span").remove();
	  }
	  
	  if( !loading ){
		  
		  // add new breadcrumb item
		  this.breadcrumbs_list.append($a);
	  
	  }	
	  else {
			
		  this.breadcrumbs_list.append("<span class='breadcrumb_loading'>&nbsp;&nbsp;<img src='images/ajax-loader-small.gif' /></span>");
	  }
  },
  
  validate : function(){
	  
	  var validated = true;
	  
	  this.notification.reset();
	  
	  
	  if( this.category_id == null ){
		  // add error message
		  this.notification.add( __("step4_category_validation_error") );
		  // validation failed
		  validated = false;
	  }
	  
	  if(!validated){
		  // show validation errors
		  this.notification.setError().show();
		
	  }
	  else {
		  // show success notification
		  this.notification.setSuccess().add( __("step4_category_selected") ).show();
		
	  }
	  
	  return validated;
  },
  
  getCategory : function(){
	
	  return this.category_id;
  },
  
  getPostData : function(){
	  
		var postData = {};
	
		postData.categoryId = this.getCategory();
		
		return postData;
	
  }

});

/* Step 5 - Specifications */
Application.View.Step5 =  Application.View.Step.extend({
  
  events: {
    "click a.skip"   				: "skip",
    "click a.next"   				: "next",
    "click #add_custom_attribute" 	: "add_custom_attribute",
    "click a.remove"				: "remove_custom_attribute"
  },
  
  initialize : function(){
	  
	 // initialize notification box
	  this.notification = new  Application.View.Notification({ model : this });
	  
	  // set default notification
	  this.notification.setImportant().add( __("step5_enter_attributes") ).show();
	  
	  // ajax event
	  this.ajax = null;
	  
	  // custom attibutes template that we will add for each new set of attributes
	  this.custom_attribute_template = '<div class="custom_attribute_box" ><div class="floatLeft specification"><label for="form_spec_name_{id}"> Attribute Name</label><br><input title="Enter Name" value="Enter Name" class="inputdefault form_spec_name" id="form_spec_name_{id}"></div><div class="floatLeft specification"><label for="form_spec_desc_{id}">Attribute Description</label><br><input title="Enter Description" value="Enter Description" class="inputdefault form_spec_desc" id="form_spec_desc_{id}"></div><div class="floatLeft" ><a href="#" class="remove" >Remove</a></div></div>';
	  
	  // container where all loaded attributes will go
	  this.specification_box = this.$el.find("#predefined_attributes").empty();
	  
	  // container where all custom attributes will go
	  this.custom_attributes = this.$el.find("#custom_attributes").empty();
	  
	  // if step4 triggers category changed, load attributes set for that category
	  this.model.on('category_changed',this.load_specifications,this);
	  
  },
  load_specifications : function( category ){
	  
	 var self = this;
	 
	 // delete all loaded attributes
	 this.specification_box.empty();
	 
	 // if category is null, stop
	 if(category === null) return;
	 
	 // if ajax is already running stop
	 if(this.ajax!==null){
		 this.ajax.abort();
	 }
	 
	  // set loading notification
	  this.notification.reset().setImportant().add( __("step5_attributes_loading") ).show();
	this.ajax = $.getJSON('http://data.epek.com/US_EN/' + category.CategoryPath + '/attribute', function (r) {
		self.render_specifications(r);
	}).complete(function () {
		this.ajax = null;
	});
  },
  render_specifications : function( specifications ){
	  
	  var self = this;
	  $.each( specifications.NameRecommendation ,function(i,specification){
		// to match old structure from cms
		var spec = {'name': specification.Name, 'id': 's' + i, 'values': {}};
		$.each(specification.ValueRecommendation || [], function (i, recommendation) {
			spec.values['r' + i] = recommendation.Value;
		});

		 
		 // attribute container
		 var $specification = $("<div class='floatLeft specification' ></div>");
		 
		 
		 // generate attributes input html
		$specification.append('<label for="spec_' + spec.id + '">' + spec.name + '</label><br>');
		var $input = $('<input type="text" id="spec_' + spec.id + '" class="'+( i % 3 === 1 ? "no_right_margin" : "" ) + '" />').data('data', spec).appendTo($specification);
		$specification.appendTo(self.specification_box);
		new Application.View.Autocomplete({
			appendTo : $specification,
			items : spec.values
		});
	  });
	  
	
	  // set default notification
	  this.notification.reset().setImportant().add( __("step5_enter_attributes") ).show();
  },
  add_custom_attribute : function(){
	  
	  // generate random ID
	  var id = new Date().getTime();
	  
	  // load template, and add custom ID in it
	  var template = $(this.custom_attribute_template.replace(/\{id\}/g,id));

      template.find(":input").each(function(){
		$(this).data("default",$(this).attr("title"));
	  });
	  
	  // append custom attributes into custom attributes container
	  this.custom_attributes.append(template);
		 
	  // prevent event from bubbling and cancel default action
	  return false;
  },
  remove_custom_attribute : function( event ){
	  
	  // remove clicked custom attribute
	  $(event.currentTarget).parents("div.custom_attribute_box").remove();
	  
	  // prevent event from bubbling and cancel default action
	  return false;
  },
  
  validate : function(){
	  
	  return true;
	  
  },
  
  getPostData : function(){
	  
		var postData = {attributes: {standard: [], custom: []}};
		
		postData.attributes.standard.push({'name': "Condition", 'value': this.model.steps[2].inputs.condition.val()});
		
		this.specification_box.find("input").each(function(){
			
			if($.trim($(this).val())!==''){
				var o ={'name': $(this).data('data').name, 'value': $(this).val()};
				postData.attributes.standard.push(o);
			}
			
		});
		
		this.custom_attributes.children().each(function(){
			
			if($.trim( $(this).find("input.form_spec_name").val() )!=='' && $.trim( $(this).find("input.form_spec_desc").val() )!=='' ){
				var o ={'name': $(this).find("input.form_spec_name").val(), 'value': $(this).find("input.form_spec_desc").val()};
				postData.attributes.custom.push(o);
			}
			
		});
		
		
		return postData;
	
  }

});

/* Step 6 - Listing Type */
Application.View.Step6 =  Application.View.Step.extend({
  
  events: {
    "click a.skip"   					: "skip",
    "click a.next"   					: "next",
    "click #set_type_auction" 			: "selectAuction",
    "click #set_type_fixed"	  			: "selectFixed",
    "change #form_list_2,#form_list_4" 	: "formatPrice",
    "change #form_list_6" 				: "formatQuantity",
    "keyup #form_list_6" 				: "toggleAdditionalCosts"
  },
  
  initialize : function(){
	  
	  // initialize notification box
	  this.notification = new  Application.View.Notification({ model : this });
	  
	  // set default notification
	  this.notification.setImportant().add( __("step6_select_auction_type") ).show();
	  
	  // auction fields
	  this.auction_fields = this.$el.find(".auction_only_fields");
	  
	  // fixed price fields
	  this.fixed_price_fields = this.$el.find(".fixed_only_fields");
	  
	  this.inputs = {};
	  
	  // starting price input
	  this.inputs.starting_price = this.$el.find("#form_list_2");
	  
	  // price input
	  this.inputs.price = this.$el.find("#form_list_4");
	  
	  // quantity input
	  this.inputs.quantity = this.$el.find("#form_list_6");
	  
	  this.inputs.auction_duration = this.$el.find("#form_list_3");
	  
	  this.inputs.fixedprice_duration = this.$el.find("#form_list_5");
	  
	  this.toggleAdditionalCosts();
	  
	  // auction price
	  this.selected_type = null;
	  
  },
  
  formatQuantity : function( event ){
	  
	  // get input element
	  var input = $(event.currentTarget);
	  
	  // get input value as float
	  var inputValue = parseFloat(input.val());
	  
	  // if input value is not number , set it to ""
	  if(isNaN(inputValue)){
		  input.val("");
	  }
	  else {
		  input.val(Math.abs(inputValue));
	  }
  },
  
  formatPrice : function( event ) {
	  
	  // get input element
	  var input = $(event.currentTarget);
	  
	   // get input value as float
	  var inputValue = parseFloat(input.val().replace(/^\$/,""));
	  
	  // if input value is not number , set it to "$"
	  if(isNaN(inputValue)){
		  input.val("");
	  }
	  else {
		  input.val(""+Math.abs(inputValue));
	  }
  },
  
  selectFixed : function( event ){
	  
	  // deselect all auction buttons
	  this.$el.find("input.faux_radio").removeClass('selected');
	  
	  // select clicked button
      $(event.currentTarget).addClass('selected');
      
      // set type
	  this.selected_type = "fixed_price";
	  
	  // hide auction input fields
	  this.auction_fields.hide();
	  
	  // show fixed price input fields
	  this.fixed_price_fields.show();
	  
	  // show next step
	  this.$el.find(".hr_next_step").fadeIn();
	  
	  // show fixed price notification
	  this.notification.reset().setImportant().add( __("step6_enter_price_and_quantity") ).show();
	  
	  this.toggleAdditionalCosts();
  },
  
  selectAuction : function( event ){
	  
	  // deselect all auction buttons
	  this.$el.find("input.faux_radio").removeClass('selected');
	  
	  // select clicked button
      $(event.currentTarget).addClass('selected');
      
       // set type
	  this.selected_type = "auction";
	  
	  // hide fixed price input fields
	  this.fixed_price_fields.hide();
	  
	   // show auction input fields
	  this.auction_fields.show();
	  
	  // show next step
	  this.$el.find(".hr_next_step").fadeIn();
	  
	  // show auction notification
	  this.notification.reset().setImportant().add( __("step6_enter_starting_price") ).show();
	  
	  this.toggleAdditionalCosts();
  },
  
    validate : function(){
	  
	  var validated = true;
	  
	  this.notification.reset();
	  
	  switch( this.selected_type ){
		
		 case "auction" :
		 
			if( isNaN(this.getStartingPrice()) || !this.getStartingPrice() > 0 ){
				
				validated = false;
				
				this.notification.add( __("step6_starting_price_validation_error") );
				 
				this.inputs.starting_price.errorInput();
			}
			
		 break;
		 
		 case "fixed_price" :
		 
			if( isNaN(this.getPrice()) || !this.getPrice() > 0 ){
				
				validated = false;
				
				this.notification.add( __("step6_price_validation_error") );
				 
				this.inputs.price.errorInput();
			}
		 
		 
			if( isNaN(this.getQuantity()) || !this.getQuantity() > 0 ){
				
				validated = false;
				
				this.notification.add( __("step6_quantity_validation_error") );
				 
				this.inputs.quantity.errorInput();
			}
		 
		 break;
		
		 default : 
		 
			validated = false;
			
			this.notification.add( __("step6_auction_type_validation_error") );
			
		 break;  
		  
	  }
	  
	  if(validated){
		  // show success notification
		  this.notification.setSuccess().add( __("step6_validated") ).show();
	  }
	  else {
		  this.notification.setValidationError().show();
	  }
	  
	  return validated;
  },
  
  toggleAdditionalCosts : function(){
	  
	  if( this.getQuantity() > 1 && this.selected_type == "fixed_price" ){
		  this.model.trigger("additionalCost");
	  }
	  else {
		  this.model.trigger("noAdditionalCost");
	  }
  },
  
  getStartingPrice : function(){
	
	return Number(this.inputs.starting_price.val().replace("$",""));
  },
  
  getPrice : function(){
	
	return Number(this.inputs.price.val().replace("$",""));
  },
  
  getQuantity : function(){
	
	return parseInt(this.inputs.quantity.val());
  },
  
  getDuration : function(){
	  
		var duration = '';
		
		if(this.selected_type == "auction"){
			 duration = this.inputs.auction_duration.val();
		}
		else {
			 duration = this.inputs.fixedprice_duration.val();
		}
		
		return duration;
  },
  
  getPostData : function(){
	  
		var postData = {price: {}};
		
		if(this.selected_type == "auction"){
			postData.price.startPrice = this.getStartingPrice();
			postData.duration = this.getDuration();
		}
		else {
			postData.price.fixedPrice = this.getPrice();
			postData.quantity = this.getQuantity();
			postData.duration= this.getDuration();
		}
		
		return postData;
	
  }


});

/* Step 7 - Shipping Details */
Application.View.Step7 =  Application.View.Step.extend({
  
  events: {
    "click a.skip"   				: "skip",
    "click a.next"   				: "next",
    "click #add_another_shipping" 	: "add_shipping",
    "click .form_ship_1_5" 			: "toggle_additional_info",
    "click .remove" 				: "remove_shipping",
    "change .form_ship_3" 			: "formatPrice",
    "change .form_ship_6" 			: "formatPrice",
    "click span.checkbox" 			: "toggle_pickup"
  },
  
  initialize : function(){
	  
	  var self = this;
	  
	 // initialize notification box
	  this.notification = new  Application.View.Notification({ model : this });
	
	  this.notification.add( __( "step7_loading_shipping" ) );
	  
	  // shipping types
	  this.shippings = {
		  universal   : new Array(),
		  standard 	  : new Array(),
		  expedited   : new Array(),
		  economy 	  : new Array(),
		  oneday 	  : new Array(),
		  other		  : new Array(),
		  promotional : new Array(),
		  delivery 	  : new Array(),
		  pickup	  : new Array()
	  }
	  
	  // this will be shipping template that we will use to generate additional shipping options
	  this.shipping_template='';
	  
	  this.showAdditionalCost = false;
	  
	  this.model.bind("noAdditionalCost",function(){
		  
		  self.showAdditionalCost = false;
		  
		  self.shipping_wrapper.find(".shippingAdditionalCost").hide();
		  
	  });
	  
	  this.model.bind("additionalCost",function(){
		  
		  self.showAdditionalCost = true;
		  
		  self.shipping_wrapper.find(".shippingAdditionalCost").show().find("input");
		  
	  });
	  
	  // shipping container
	  this.shipping_wrapper = this.$el.find("#ship_forms");
	  
	// get all shippings
	$.getJSON('http://data.epek.com/US_EN/shipping', function (response) {
		// generate shipping template from JSON response
		self.create_shipping_template( response );

		// add one shipping box
		self.add_shipping();
	})
  },
  
  formatPrice : function( event ) {
	  
	  // get input element
	  var input = $(event.currentTarget);
	  
	   // get input value as float
	  var inputValue = parseFloat(input.val().replace(/^\$/,""));
	  
	  // if input value is not number , set it to "$"
	  if(isNaN(inputValue)){
		  input.val("");
	  }
	  else {
		  input.val(""+Math.abs(inputValue));
	  }
  },
  
  create_shipping_template : function(response){

	  var self = this;
	  
	  $.each( response , function( i, shipping ){
		  
			// populate shipping categories arrays so that we can nicely arange them with optgroup
			switch( shipping.category ){
				
				case "STANDARD" :
					self.shippings.standard.push( shipping );
				break;
				
				case "EXPEDITED" :
					self.shippings.expedited.push( shipping );
				break;
				
				case "ECONOMY" :
					self.shippings.economy.push( shipping );
				break;
				
				case "ONE_DAY" :
					self.shippings.oneday.push( shipping );
				break;
				
				case "OTHER" :
					self.shippings.other.push( shipping );
				break;
				
				case "PROMOTIONAL" :
					self.shippings.promotional.push( shipping );
				break;
				
				case "DELIVERY" :
					self.shippings.delivery.push( shipping );
				break;
				
				case "PICKUP" :
					self.shippings.pickup.push( shipping );
				break;
				
				default : 
				
					self.shippings.universal.push( shipping );
				break;
			}
	  });
	  
	  // sort alphabetically all shipping types
	  for( var i in this.shippings ){
		  this.shippings[i].sort();
	  }
	  
	  // generate shipping template
	  this.shipping_template +='<div class="ship_form">';
	  this.shipping_template +='<a class="remove" href="#">Remove Shipping Option</a>';
											
	  this.shipping_template +='<label for="form_ship_{id}_1">Shipping Service</label><br>';
	  this.shipping_template +='<select class="full" id="form_ship_{id}_1">';
	  this.shipping_template +='<option></option>';

	  for( var shipping_category in this.shippings ){
		  
		  var options = '';
		  
		  if( this.shippings[shipping_category].length > 0 ){
			 $.each( this.shippings[shipping_category],  function( i, item ){
				 var description = item.description;
				 if( item.timeMax > 0 && item.timeMax > item.timeMin ){
					 description += " ( "+item.timeMin+" to "+(item.timeMax==1?"1 day":item.timeMax+" days")+ " )";
				 }
				 options +='<option value="'+item.name+'">'+description+'</option>';
			 });
		  }
		  
		  switch( shipping_category ){
			  				
				case "standard" :
					options = "<optgroup label='Standard Services' >" + options + "</optgroup>";
				break;
				
				case "expedited" :
					options = "<optgroup label='Expedited Services' >" + options + "</optgroup>";
				break;
				
				case "economy" :
					options = "<optgroup label='Economy Services' >" + options + "</optgroup>";
				break;
				
				case "oneday" :
					options = "<optgroup label='One Day Services' >" + options + "</optgroup>";
				break;
				
				case "pickup" :
					options = "<optgroup label='Pickup Services' >" + options + "</optgroup>";
				break;
				
				default : 
				
				break;
		  }
		  
		  this.shipping_template += options;
	  }
	  
	  this.shipping_template +='</select>';

	  this.shipping_template +='<div class="floatLeft">';
	  this.shipping_template +='<label for="form_ship_{id}_2">Handling Time</label><br>';
	  this.shipping_template +='<select class="form_ship_2" id="form_ship_{id}_2">';
	  this.shipping_template +='<option></option>';
	  this.shipping_template +='<option value="3" selected="">3 Days</option>';
	  this.shipping_template +='<option value="2">2 Days</option>';
	  this.shipping_template +='<option value="1">1 Days</option>';
	  this.shipping_template +='</select>';
	  this.shipping_template +='</div>';

	  this.shipping_template +='<div class="floatLeft">';
	  this.shipping_template +='<label for="form_ship_{id}_3">Shipping Cost</label><br>';
	  this.shipping_template +='<input value="" class="form_ship_3 dolar" id="form_ship_{id}_3">';
	  this.shipping_template +='</div>';

	  this.shipping_template +='<div class="floatLeft shippingAdditionalCost">';
	  this.shipping_template +='<label for="form_ship_{id}_6">Each additional</label><br>';
	  this.shipping_template +='<input value="" class="form_ship_6 dolar" id="form_ship_{id}_6">';
	  this.shipping_template +='</div>';

	  this.shipping_template +='<div class="clear"></div>';
	  
	  this.shipping_template +='<div class="floatLeft buyer_pickup_checkbox_area">';
	  this.shipping_template +='<span class="checkbox">';
	  this.shipping_template +='Allow buyer to pick up the item from you?';
	  this.shipping_template +='<input type="checkbox" id="form_ship_{id}_4">';
	  this.shipping_template +='</span>';
	  this.shipping_template +='</div>';

	  this.shipping_template +='<div class="clear"></div>';

	  this.shipping_template +='<label class="form_ship_1_5" for="form_ship_{id}_5">Additional Payment Instructions <span>( optional )</span></label><br>';
	  this.shipping_template +='<input class="full" id="form_ship_{id}_5" class="input_ship_1_5">';
		
	  this.shipping_template +='</div>';
	  
  },
  
  add_shipping : function( ){
	  
	  // if we already don't have two shipping methods placed
	  if(this.shipping_wrapper.children().size()<2){
		  
		  this.notification.reset().setImportant().add( __( "step7_enter_shipping_info" ) ).show();
	  
		  var id = this.shipping_wrapper.children().size() + 1;
		  
		  // if this is second shipping, hide ADD SHIPPING button
		  if( id == 2 ) this.$el.find("#add_another_shipping").hide();
		  
		  // get shipping template and insert unique ID in it
		  var template = $(this.shipping_template.replace(/\{id\}/g,id));
		  
		  // if this is first shipping method , remove the button
		  if(this.shipping_wrapper.children().size()==0){
			  template.find("a.remove").remove();
		  }
		  else {
			  template.find("select.form_ship_2").parent().remove();
			  template.find(".buyer_pickup_checkbox_area").remove();
			  template.find("label.form_ship_1_5").remove();
			  template.find("input.input_ship_1_5").remove();
		  }
		  
		  if( !self.showAdditionalCost ){
			  template.find(".shippingAdditionalCost").hide();
		  }
		  
		  // add new shipping form
		  this.shipping_wrapper.append(template);
	  
	 }
	  
	  return false;
  },
  
  remove_shipping : function( event ){
	  
	  // if there are more that one shipping method, allow deletion
	  if(this.shipping_wrapper.children().size()>1){
		  
		$( event.currentTarget ).parent().remove();
	  
		this.$el.find("#add_another_shipping").show();
	  
	  }
	  
	  return false;
	  
  },
  
  toggle_additional_info : function( event ){
	  
	var additional_info = $( event.currentTarget );
	
	if (additional_info.hasClass('open')) {
		// close it
		$("#"+additional_info.attr("for")).blur().hide();
		additional_info.removeClass('open');
	} else {
		additional_info.addClass('open');
		$("#"+additional_info.attr("for")).show().focus();
	}
	
	return false;
  },
  
  toggle_pickup : function( event ){
	  
	var checkbox = $( event.currentTarget );
	
	if (checkbox.hasClass('checked')) {
		checkbox.removeClass('checked');
		checkbox.find("input").removeAttr("checked"); 
	} else {
		checkbox.addClass('checked');        
		checkbox.find("input").attr("checked","checked");    
	}
  },
  
  
    validate : function(){
	  
	  var validated = true;
	  
	  this.notification.reset();
	 
	 // get all shipping inputs , except checkbox and additional info
	  this.shipping_wrapper.find(":input").filter(function(){ return !this.id.match(/^form_ship_[0-9]+_(5|4|6)$/);}).each(function(){
		
			if( trim($(this).val().replace(/^\$/,"")) == "" ){
				validated = false;
				$(this).errorInput();
			}
	  });
	  
	  if(validated){
		  // show success notification
		  this.notification.setSuccess().add( __("step7_validated") ).show();
	  }
	  else {
		  this.notification.add( __("step7_shipping_validation_error") ).setError().show();
	  }
	  
	  return validated;
  },
  
  getShippingService : function( $form ){
	  
	  return $form.find("select:first").val();
  },
  
  getShippingTime : function( $form ){
	  
	  return $form.find("select:eq(1)").val();
  },
  
  getShippingCost : function( $form ){
	  
	  return Number($form.find("input.form_ship_3").val());
  },
  
  getShippingAdditionalCost : function( $form ){
	  
	  return Number($form.find("input.form_ship_6").val());
  },
  
  getLocalPickup : function( $form ){
	  
	  return $form.find("input[type='checkbox']:first").prop("checked")?true:false;
  },
  
  shippingPaymentInstructions : function( $form ){
	  
	  return $form.find("input:last").val();
  },
  
  getPostData : function(){
	  
		var self = this;
		var postData = new Array();
		
		postData.shippingService = [];
		//postData.shippingCost = [];
		if( self.showAdditionalCost ){
			postData.shippingServiceAdditionalCost = [];
		}
		this.shipping_wrapper.children().each(function( i , shippingForm ){ 
			shippingForm = $(shippingForm);
			var o = {
				shippingService: self.getShippingService( shippingForm ),
				dispatchTimeMax: self.getShippingTime( shippingForm ),
				shippingCost: self.getShippingCost( shippingForm )
			};
			postData.shippingService.push(o);
			/*
			if( i === 0 ){
				postData.dispatchTimeMax = self.getShippingTime( shippingForm ) ;
				postData.localPickup = self.getLocalPickup( shippingForm );
				postData.shippingPaymentInstructions = self.shippingPaymentInstructions( shippingForm );
			}
			
			postData.shippingService.push( self.getShippingService( shippingForm ) );
			postData.shippingCost.push( self.getShippingCost( shippingForm ) );
			if( self.showAdditionalCost ){
				postData.shippingServiceAdditionalCost.push( self.getShippingAdditionalCost( shippingForm ) );
			}
			*/
		});
		
		return postData;
	
  }
  


});

/* Step 8 - Payment Methods */
Application.View.Step8 =  Application.View.Step.extend({
  
  events: {
    "click a.skip"   				: "skip",
    "click a.next"   				: "next",
    "click ul.nav a" 				: "show_payment_method",
    "click #list_item_button"		: "list_item",
    "click .credit_box" 			: "toggle_credit_card",
    "change .payment_details input" : "toggle_methods",
    "keyup .payment_details input" 	: "toggle_methods",
    "click a.remove"				: "remove_method",
    "click span.checkbox.pickup" 	: "toggle_pickup",
    "click span.checkbox.banktransfer" : "toggle_bank_transfer"
  },
  
  initialize : function(){
	  
	 // initialize notification box
	  this.notification = new  Application.View.Notification({ model : this });
	  
	  this.notification.setImportant().add( __("step8_enter_payment_info") ).show();
	  
	  this.nav = this.$el.find("ul.nav a");
	  
	  this.indicators = this.nav.find(".indicator");
	  
	  this.methods = this.$el.find("div.payment_details");
	  
	  this.inputs = {};
	
	  // paypal
	  this.inputs.paypal_email = this.$el.find("#form_pay_5");
	  // google
	  this.inputs.google_email = this.$el.find("#form_pay_6"),
	  // cheque
	  this.inputs.cheque = this.$el.find("#form_pay_7");
	  
	  this.selectedMethods = {
		  paypal : {
			  selected : false,
			  email : ''
		  },
		  credit_cards : {
			  "Visa" : false,
			  "MasterCard" : false,
			  "American Express" : false,
			  "Discover" : false
		  },
		  google : {
			  selected : false,
			  email : ''
		  },
		  cheque : {
			  selected : false,
			  name : ""
		  },
		  other : {
			  bank_transfer : false,
			  pay_on_pickup : false
		  }
	  }
	  
	  
  },
  
  list_item : function(){
	
	this.model.listItem();
	
	return false;  
  },
  
  show_payment_method : function( event ){
	  
	  this.nav.not( event.currentTarget ).removeClass("selected");
	  
	  $( event.currentTarget ).addClass("selected");
	  
	  var targetMethod = this.methods.eq( $( event.currentTarget ).parent().index() );
	  
	  this.methods.not( targetMethod ).removeClass("selected");
	  
	  targetMethod.addClass("selected");
	  
	  targetMethod.find("input:first").focus();
	  
	  return false;
  },
  
  toggle_methods : function( event ){
	
	if (event.currentTarget.value == '') {
		// turn off indicator
		switch( event.currentTarget.id ){
			
			case "form_pay_5":
				this.selectedMethods.paypal.selected = false;
				this.selectedMethods.paypal.email = '';
			break;
			case "form_pay_6":
				this.selectedMethods.google.selected = false;
				this.selectedMethods.google.email = '';
			break;
			case "form_pay_7":
				this.selectedMethods.cheque.selected = false;
				this.selectedMethods.cheque.name = '';
			break;
			
		}
		
		this.indicators.filter("#"+event.currentTarget.id+"_indicator").removeClass('selected');        
	} else {
		
		switch( event.currentTarget.id ){
			
			case "form_pay_5":
				this.selectedMethods.paypal.selected = true;
				this.selectedMethods.paypal.email = event.currentTarget.value;
			break;
			case "form_pay_6":
				this.selectedMethods.google.selected = true;
				this.selectedMethods.google.email = event.currentTarget.value;
			break;
			case "form_pay_7":
				this.selectedMethods.cheque.selected = true;
				this.selectedMethods.cheque.name = event.currentTarget.value;
			break;
			
		}
		// turn on indicator
		this.indicators.filter("#"+event.currentTarget.id+"_indicator").addClass('selected');   
	}  
	
	this.notification.reset().setImportant().add( __("step8_enter_payment_info") ).show();
	
  },
  
  remove_method : function( event ){
	  
	  $( event.currentTarget).siblings("input").val("").trigger("change");
	  
	  
	  $( event.currentTarget).siblings("span.checkbox.checked").trigger("click");
	  
	  this.$el.find("ul.nav li > a.selected").removeClass("selected"); 
	  
	  $( event.currentTarget ).parents(".selected").removeClass("selected");
	  
	  return false;
  },
  
  toggle_credit_card : function( event ){
	  
	  var card = $( event.currentTarget );
	  
	  if (card.hasClass('selected')) {
		  card.removeClass('selected');
		  card.children('.image').css('opacity', '1').fadeTo(500, 0.2);
		
		  this.selectedMethods.credit_cards[card.children('input').val()] = false;
		  
		  // turn off indicator
		   this.indicators.filter("#"+card.children('input').attr('id')+"_indicator").removeClass('selected');      
		        
	  } else {
		  
		  card.addClass('selected');
		  card.children('.image').css('opacity', '0.2').fadeTo(500, 1);
		  
		  this.selectedMethods.credit_cards[card.children('input').val()] = true;
		  // turn on indicator
		   this.indicators.filter("#"+card.children('input').attr('id')+"_indicator").addClass('selected');  
	  }
  },
  
  toggle_pickup : function( event ){
	  
	var checkbox = $( event.currentTarget );
	
	if (checkbox.hasClass('checked')) {
		checkbox.removeClass('checked');
		checkbox.find("input").removeAttr("checked"); 
		this.selectedMethods.other.pay_on_pickup = false;
	} else {
		checkbox.addClass('checked');        
		checkbox.find("input").attr("checked","checked");    
		this.selectedMethods.other.pay_on_pickup = true;
	}
	
	if(this.selectedMethods.other.pay_on_pickup ){
		
		// turn on indicator
		this.indicators.filter("#form_pay_9_indicator").addClass('selected');   
	}
	else {
		
		// turn on indicator
		this.indicators.filter("#form_pay_9_indicator").removeClass('selected');   
	}
  },
  
  toggle_bank_transfer : function( event ){
	  
	var checkbox = $( event.currentTarget );
	
	if (checkbox.hasClass('checked')) {
		checkbox.removeClass('checked');
		checkbox.find("input").removeAttr("checked"); 
		this.selectedMethods.other.bank_transfer = false;
	} else {
		checkbox.addClass('checked');        
		checkbox.find("input").attr("checked","checked");    
		this.selectedMethods.other.bank_transfer = true;
	}
	
	if( this.selectedMethods.other.bank_transfer){
		
		// turn on indicator
		this.indicators.filter("#form_pay_8_indicator").addClass('selected');   
	}
	else {
		
		// turn on indicator
		this.indicators.filter("#form_pay_8_indicator").removeClass('selected');   
	}
  },
  validate : function(){
		  
	  var validated = true;
	  
	  this.notification.reset();
	 
	  if( ! ( this.selectedMethods.paypal.selected || 
		  this.selectedMethods.credit_cards.Visa || 
		  this.selectedMethods.credit_cards.MasterCard || 
		  this.selectedMethods.credit_cards.Discover || 
		  this.selectedMethods.credit_cards["American Express"] || 
		  this.selectedMethods.cheque.selected || 
		  this.selectedMethods.google.selected || 
		  this.selectedMethods.other.bank_transfer ||
		  this.selectedMethods.other.pay_on_pickup  ) ){
		  
		  validated =false;
		  
		  this.notification.add( __("step8_payment_validation_error") );
	  }
	  
	  if( this.selectedMethods.paypal.selected && !this.inputs.paypal_email.val().match(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/) ){
		  
		  validated =false;
		  
		  this.notification.add( __("step8_paypal_email_validation_error") );
		  
		  this.inputs.paypal_email.errorInput();
		  
	  }
	
	  
	  if( this.selectedMethods.cheque.selected && trim(this.inputs.cheque.val()).length<3 ){
		  
		  validated =false;
		  
		  this.notification.add( __("step8_cheque_validation_error") );
		  
		  this.inputs.cheque.errorInput();
	  }
	  
	  if( this.selectedMethods.google.selected && !this.inputs.google_email.val().match(/^[0-9]+$/) ){
		  
		  validated =false;
		  
		  this.notification.add( __("step8_google_merchant_id_validation_error") );
		  
		  this.inputs.google_email.errorInput();
	  }
	  
	  if(validated){
		  // show success notification
		  this.notification.setSuccess().add( __("step8_validated") ).show();
	  }
	  else {
		  
		  this.notification.setValidationError().show();
	  }
	  
	  return validated;  
  },
  
  getPostData : function(){
	  
		var postData = {};
		
		postData.paymentMethods = [];
		
		if( this.selectedMethods.credit_cards.Visa ){
			postData.paymentMethods.push({
				paymentService:'visa' 
			});
		}
		
		if( this.selectedMethods.credit_cards.MasterCard ){
			postData.paymentMethods.push({
				paymentService:'mastercard' 
			});
		}
		
		if( this.selectedMethods.credit_cards['American Express'] ){
			postData.paymentMethods.push({
				paymentService:'american_express' 
			});
		}
		
		if( this.selectedMethods.credit_cards.Discover ){
			postData.paymentMethods.push({
				paymentService:'discover' 
			});
		}
		
		if( this.selectedMethods.google.selected ){
			postData.paymentMethods.push({
				paymentService:'googleCheckout' ,
				paymentId : this.inputs.google_email.val()
			});
		}
		
		if( this.selectedMethods.paypal.selected ){
			postData.paymentMethods.push({
				paymentService:'paypal' ,
				paymentId : this.inputs.paypal_email.val()
			});
		}
		
		if( this.selectedMethods.cheque.selected ){
			postData.paymentMethods.push({
				paymentService:'moneyOrderCheque' ,
				paymentId : this.inputs.cheque.val()
			});
		}
		
		if( this.selectedMethods.other.bank_transfer ){
			postData.paymentMethods.push({
				paymentService:'bankAccount'
			});
		}
		
		if( this.selectedMethods.other.pay_on_pickup ){
			postData.paymentMethods.push({
				paymentService:'payOnPickup' ,
			});
		}
		
		return postData;
	
  }


});


/* Final Step */
Application.View.FinalStep =  Application.View.Step.extend({
  
  events: {
	"click .list_another_item a" : "listAnotherItem",
	"click .social a" : "social"
  },
  
  initialize : function(){
	  
	 // initialize notification box
	  //this.notification = new  Application.View.Notification({ model : this });
	$social.check();
	 
  },
  
  generate : function ( data ) {
	  this.finalData = data;
	  
	  this.reset();
	  
	  var defaults = {
		  title : "",
		  description : "",
		  url : "",
		  image : "http://offer.staticpek.com/images/no_photo.png",
		  shipping : "",
		  price : "",
		  duration : 0
	  };
	  
	  var vars = $.extend( defaults , data );

	  this.finalData = vars;
	  
	  this.$el.find(".name").html( vars.title );
	  
	  this.$el.find(".description").html( vars.description );
	  
	  this.$el.find(".price").html( '<span class="currency">$</span>'+vars.price );
	  
	  this.$el.find(".shipping").html( "+ $"+vars.shipping+" Shipping and Handling" );
	  
	  this.$el.find(".expire").html( (vars.duration-1)+" days and 23 hours left" );

	  this.$el.find(".image").append("<img width='100' src='"+vars.image+"' />");
	  
	  this.$el.find("article.result").bind("click",function(e){
		  e.preventDefault();
		  
		  window.location.href = vars.url;
	  });
	  
  },
  reset : function (){
	  
	  this.$el.find(".name").empty();
	  
	  this.$el.find(".description").empty();
	  
	  this.$el.find(".price").empty();
	  
	  this.$el.find(".shipping").empty();
	  
	  this.$el.find(".image").empty();
	  
	  this.$el.find(".expire").empty();
	  
	  this.$el.find("article.result").unbind("click");
	  
  },
  social : function(e){
	var self = this;
	var service;
	var $t = $(e.currentTarget);
	if ($t.is('.facebook')) {
		service = 'facebook';
	} else if ($t.is('.twitter')) {
		service = 'twitter';
	}
	$overlay.show({
		html: Handlebars.compile($('#share-tpl').html())(self.finalData || {}),
		formSend: function (form) {
			var $form = $(form);
			$form.find('> p').removeClass('error').find('span').text('');
			var services = [];
			var fb_a = $form.find('a.facebook');
			if (fb_a.is('.selected')) {
				services.push('facebook');
			}
			var tw_a = $form.find('a.twitter');
			if (tw_a.is('.selected')) {
				services.push('twitter');
			}
			if (services.length == 0) {
				$form.find('> p').addClass('error').find('span').text('You must select at least 1 service.');
			} else {
				$form.addClass('loading');
				$social.post({
					services: services,
					title: $form.find('input[name=title]').val(),
					message: $form.find('textarea').val(),
					image: $form.find('input[name=image]').val(),
					url: $form.find('input[name=url]').val()
				}, function () {
					$form.removeClass('loading');
					$overlay.send();
				});
			}
		}
	});
	$social.connect(service, function () {
		$overlay.update({
			html: Handlebars.compile($('#share-tpl').html())(self.finalData || {})
		});
	});
	
	return false;
	
  },
  listAnotherItem : function(){
	  
	  window.location.reload();
	  
	  return false;
	  
  }

});
/* Routers */

 Application.Router.Nav = Backbone.Router.extend({
	
  initialize : function( options ){
	  this.main = options.model;
  },
  routes: {
    "!step:step_num":  "step",
    ""				: "empty",
    "!final" 		: "final"
  },
  // if hash is empty , navigate to Step 1
  empty : function(){
	this.navigate("!step1",{ trigger : true, replace : true });
  },
  // if step appeared in hash, navigate to it
  step : function( index ){
	  
	  var stepIndex = Number(index) - 1;
	  
	  // if stepIndex is a number and it exists, navigate to it
	  if(stepIndex >= 0 && stepIndex < this.main.totalSteps()){
		  this.main.goToStep( stepIndex );
	  }
  },
  'final' : function(){
	  this.main.goToFinalStep();
  }

});

/* APPLICATION INITIALIZATION */
var APP;

$(document).ready(function(){
	APP = new Application.Model.Main();
});

})(jQuery);

// Handlebar helpers
Handlebars.registerHelper('has_social', function(options) {
	if ($social.has(options.hash.service)) {
		return 'selected';
	}
});

